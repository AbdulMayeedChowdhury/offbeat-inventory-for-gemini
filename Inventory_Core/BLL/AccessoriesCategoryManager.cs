﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inventory_Core.DAL;
namespace Inventory_Core.BLL
{
    public class AccessoriesCategoryManager
    {
        private OffbeatInventoryEntities _entities;

        public AccessoriesCategoryManager()
        {
            _entities = new OffbeatInventoryEntities();    
        }

        public List<AccessoriesCategory> GetAccessoriesCategories(bool isActiveOnly)
        {
            var categories = new List<AccessoriesCategory>();

            if (isActiveOnly)
            {
                categories = _entities.AccessoriesCategories.Where(a => a.IsActive).ToList();
            }
            else
            {
                categories = _entities.AccessoriesCategories.ToList();
            }

            return categories.OrderBy(c=>c.DisplayOrder).ToList();
        }

        public AccessoriesCategory GetAccessoriesCategory(Int16 categoryId)
        {
            var accessory = _entities.AccessoriesCategories.FirstOrDefault(a => a.CategoryId == categoryId);

            return accessory;
        }

        public void SaveAccessoriesCategory(AccessoriesCategory category)
        {
            try
            {
                using (_entities)
                {
                    Int16 lastAccessoryId = _entities.AccessoriesCategories.Max(a => a.CategoryId);

                    Int16 newId = (Int16) (lastAccessoryId + 1);

                    category.CategoryId = newId;

                    _entities.AddToAccessoriesCategories(category);
                    _entities.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }
    }
}
