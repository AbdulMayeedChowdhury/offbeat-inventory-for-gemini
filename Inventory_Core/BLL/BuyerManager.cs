﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class BuyerManager
    {
        private OffbeatInventoryEntities _entities;

        public BuyerManager()
        {
            _entities = new OffbeatInventoryEntities();    
        }

        public List<Buyer> GetBuyers()
        {
            var buyers = _entities.Buyers.ToList();
            
            return buyers;
        }

        public Buyer GetBuyer(int buyerId)
        {
            var buyer = _entities.Buyers.FirstOrDefault(b => b.BuyerId == buyerId);

            return buyer;
        }

        public void SaveBuyer(Buyer buyer)
        {
            try
            {
                using (_entities)
                {
                    _entities.AddToBuyers(buyer);
                    _entities.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }
    }
}
