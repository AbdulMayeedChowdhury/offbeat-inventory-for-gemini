﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Inventory_Core.DAL;
using Inventory_Core.BLL;

namespace Inventory_GUI
{
    public partial class AccessorySetupForm : BaseForm
    {
        private string _accessoryId;

        public AccessorySetupForm()
        {
            InitializeComponent();
        }

        private void AccessorySetupForm_Load(object sender, EventArgs e)
        {
            LoadCategories();
            LoadAccessories();
            ClearFields();
        }

        private void LoadCategories()
        {
            try
            {
                List<AccessoriesCategory> categories = new AccessoriesCategoryManager().GetAccessoriesCategories(true);

                categories.Insert(0, new AccessoriesCategory { CategoryId = 0, CategoryName = "--Select Category--" });
                //categories.Insert(categories.Count, new AccessoriesCategory { CategoryId = "new", CategoryName = "--New Accessory--" });

                categoryComboBox.ValueMember = "CategoryId";
                categoryComboBox.DisplayMember = "CategoryName";

                categoryComboBox.DataSource = categories;
            }
            catch (Exception exception)
            {
                MessageBox.Show(@"Failed to load categories" + Environment.NewLine + @"Error: " + exception.Message);
            }
        }

        private void LoadAccessories()
        {
            try
            {
                List<Accessory> attributes = new AccessoryManager().GetAccessories(false);

                accessoriesDataGridView.AutoGenerateColumns = false;
                accessoriesDataGridView.DataSource = attributes;
            }
            catch (Exception exception)
            {
                MessageBox.Show(@"Failed to load accessories" + Environment.NewLine + @"Error: " + exception.Message);
            }
        }
        
        private void saveButton_Click(object sender, EventArgs e)
        {
            if (accessoryNameTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show(@"Enter accessory name");
                return;
            }

            if ((Int16)categoryComboBox.SelectedValue == 0)
            {
                MessageBox.Show(@"Select a category");
                return;
            }

            var accessory = new Accessory
            {
                AccessoriesId = _accessoryId,
                AccessoriesName = accessoryNameTextBox.Text.Trim(),
                CategoryId = (Int16) categoryComboBox.SelectedValue,
                IsActive = true
            };

            var accessoryManager = new AccessoryManager();

            try
            {
                accessoryManager.SaveAccessory(accessory);

                MessageBox.Show(@"Accessory saved successfully");
            }
            catch (Exception exception)
            {
                MessageBox.Show(@"Failed to save" + Environment.NewLine + @"Error: " + exception.Message);
            }

            ClearFields();
            LoadAccessories();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void accessoriesDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ClearFields();

            _accessoryId = accessoriesDataGridView.Rows[e.RowIndex].Cells["AccessoriesId"].Value.ToString();

            Accessory selectedAccessory = new AccessoryManager().GetAccessory(_accessoryId);

            if (selectedAccessory == null)
            {
                MessageBox.Show(@"Could not find Accessory.");
                _accessoryId = string.Empty;
                return;
            }

            accessoryNameTextBox.Text = selectedAccessory.AccessoriesName;
            categoryComboBox.SelectedValue = selectedAccessory.CategoryId;
        }

        private void ClearFields()
        {
            _accessoryId = string.Empty;
            accessoryNameTextBox.Text = string.Empty;
            categoryComboBox.SelectedValue = 0;
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            ClearFields();
        }
    }
}
