﻿namespace Inventory_GUI
{
    partial class StartupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dashboardButton = new System.Windows.Forms.Button();
            this.reportButton = new System.Windows.Forms.Button();
            this.logoutButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.securityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createLoginUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accessoriesSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unitSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.specificationSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderDashboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createBuyerOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeBuyerOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accessoriesRequirementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receivingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryToProductionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transferToOtherStyleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dashboardButton
            // 
            this.dashboardButton.Location = new System.Drawing.Point(3, 0);
            this.dashboardButton.Name = "dashboardButton";
            this.dashboardButton.Size = new System.Drawing.Size(69, 47);
            this.dashboardButton.TabIndex = 0;
            this.dashboardButton.Text = "Dashboard";
            this.dashboardButton.UseVisualStyleBackColor = true;
            this.dashboardButton.Click += new System.EventHandler(this.dashboardButton_Click);
            // 
            // reportButton
            // 
            this.reportButton.Location = new System.Drawing.Point(78, 0);
            this.reportButton.Name = "reportButton";
            this.reportButton.Size = new System.Drawing.Size(69, 47);
            this.reportButton.TabIndex = 1;
            this.reportButton.Text = "Report";
            this.reportButton.UseVisualStyleBackColor = true;
            this.reportButton.Click += new System.EventHandler(this.reportButton_Click);
            // 
            // logoutButton
            // 
            this.logoutButton.Location = new System.Drawing.Point(153, 0);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(69, 47);
            this.logoutButton.TabIndex = 2;
            this.logoutButton.Text = "Logout";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.securityToolStripMenuItem,
            this.configurationToolStripMenuItem,
            this.operationToolStripMenuItem,
            this.reportToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(533, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // securityToolStripMenuItem
            // 
            this.securityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createLoginUserToolStripMenuItem});
            this.securityToolStripMenuItem.Name = "securityToolStripMenuItem";
            this.securityToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.securityToolStripMenuItem.Text = "Security";
            // 
            // createLoginUserToolStripMenuItem
            // 
            this.createLoginUserToolStripMenuItem.Name = "createLoginUserToolStripMenuItem";
            this.createLoginUserToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.createLoginUserToolStripMenuItem.Text = "Create Login User";
            this.createLoginUserToolStripMenuItem.Click += new System.EventHandler(this.createLoginUserToolStripMenuItem_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemSetupToolStripMenuItem,
            this.accessoriesSetupToolStripMenuItem,
            this.unitSetupToolStripMenuItem,
            this.specificationSetupToolStripMenuItem});
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.configurationToolStripMenuItem.Text = "Configuration";
            // 
            // itemSetupToolStripMenuItem
            // 
            this.itemSetupToolStripMenuItem.Name = "itemSetupToolStripMenuItem";
            this.itemSetupToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.itemSetupToolStripMenuItem.Text = "Item Setup";
            this.itemSetupToolStripMenuItem.Click += new System.EventHandler(this.itemSetupToolStripMenuItem_Click);
            // 
            // accessoriesSetupToolStripMenuItem
            // 
            this.accessoriesSetupToolStripMenuItem.Name = "accessoriesSetupToolStripMenuItem";
            this.accessoriesSetupToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.accessoriesSetupToolStripMenuItem.Text = "Accessories Setup";
            this.accessoriesSetupToolStripMenuItem.Click += new System.EventHandler(this.accessoriesSetupToolStripMenuItem_Click);
            // 
            // unitSetupToolStripMenuItem
            // 
            this.unitSetupToolStripMenuItem.Name = "unitSetupToolStripMenuItem";
            this.unitSetupToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.unitSetupToolStripMenuItem.Text = "Unit Setup";
            this.unitSetupToolStripMenuItem.Click += new System.EventHandler(this.unitSetupToolStripMenuItem_Click);
            // 
            // specificationSetupToolStripMenuItem
            // 
            this.specificationSetupToolStripMenuItem.Name = "specificationSetupToolStripMenuItem";
            this.specificationSetupToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.specificationSetupToolStripMenuItem.Text = "Specification Setup";
            this.specificationSetupToolStripMenuItem.Click += new System.EventHandler(this.specificationSetupToolStripMenuItem_Click);
            // 
            // operationToolStripMenuItem
            // 
            this.operationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.orderDashboardToolStripMenuItem,
            this.createBuyerOrderToolStripMenuItem,
            this.closeBuyerOrderToolStripMenuItem,
            this.accessoriesRequirementToolStripMenuItem,
            this.purchaseOrderToolStripMenuItem,
            this.receivingToolStripMenuItem,
            this.deliveryToProductionToolStripMenuItem,
            this.transferToOtherStyleToolStripMenuItem});
            this.operationToolStripMenuItem.Name = "operationToolStripMenuItem";
            this.operationToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.operationToolStripMenuItem.Text = "Operation";
            // 
            // orderDashboardToolStripMenuItem
            // 
            this.orderDashboardToolStripMenuItem.Name = "orderDashboardToolStripMenuItem";
            this.orderDashboardToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.orderDashboardToolStripMenuItem.Text = "Order Dashboard";
            this.orderDashboardToolStripMenuItem.Click += new System.EventHandler(this.orderDashboardToolStripMenuItem_Click);
            // 
            // createBuyerOrderToolStripMenuItem
            // 
            this.createBuyerOrderToolStripMenuItem.Name = "createBuyerOrderToolStripMenuItem";
            this.createBuyerOrderToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.createBuyerOrderToolStripMenuItem.Text = "Create Buyer Order";
            this.createBuyerOrderToolStripMenuItem.Click += new System.EventHandler(this.createBuyerOrderToolStripMenuItem_Click);
            // 
            // closeBuyerOrderToolStripMenuItem
            // 
            this.closeBuyerOrderToolStripMenuItem.Name = "closeBuyerOrderToolStripMenuItem";
            this.closeBuyerOrderToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.closeBuyerOrderToolStripMenuItem.Text = "Close Buyer Order";
            // 
            // accessoriesRequirementToolStripMenuItem
            // 
            this.accessoriesRequirementToolStripMenuItem.Name = "accessoriesRequirementToolStripMenuItem";
            this.accessoriesRequirementToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.accessoriesRequirementToolStripMenuItem.Text = "Accessories Requirement";
            this.accessoriesRequirementToolStripMenuItem.Click += new System.EventHandler(this.accessoriesRequirementToolStripMenuItem_Click);
            // 
            // purchaseOrderToolStripMenuItem
            // 
            this.purchaseOrderToolStripMenuItem.Name = "purchaseOrderToolStripMenuItem";
            this.purchaseOrderToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.purchaseOrderToolStripMenuItem.Text = "Purchase Order";
            this.purchaseOrderToolStripMenuItem.Click += new System.EventHandler(this.purchaseOrderToolStripMenuItem_Click);
            // 
            // receivingToolStripMenuItem
            // 
            this.receivingToolStripMenuItem.Name = "receivingToolStripMenuItem";
            this.receivingToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.receivingToolStripMenuItem.Text = "Receiving";
            this.receivingToolStripMenuItem.Click += new System.EventHandler(this.receivingToolStripMenuItem_Click);
            // 
            // deliveryToProductionToolStripMenuItem
            // 
            this.deliveryToProductionToolStripMenuItem.Name = "deliveryToProductionToolStripMenuItem";
            this.deliveryToProductionToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.deliveryToProductionToolStripMenuItem.Text = "Delivery To Production";
            this.deliveryToProductionToolStripMenuItem.Click += new System.EventHandler(this.deliveryToProductionToolStripMenuItem_Click);
            // 
            // transferToOtherStyleToolStripMenuItem
            // 
            this.transferToOtherStyleToolStripMenuItem.Name = "transferToOtherStyleToolStripMenuItem";
            this.transferToOtherStyleToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.transferToOtherStyleToolStripMenuItem.Text = "Transfer To Other Style";
            this.transferToOtherStyleToolStripMenuItem.Click += new System.EventHandler(this.transferToOtherStyleToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.reportToolStripMenuItem.Text = "Report";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dashboardButton);
            this.panel1.Controls.Add(this.logoutButton);
            this.panel1.Controls.Add(this.reportButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(533, 50);
            this.panel1.TabIndex = 4;
            // 
            // StartupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 287);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "StartupForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Offbeat Inventory - Gemini Garments Limited";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.StartupForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button dashboardButton;
        private System.Windows.Forms.Button reportButton;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem securityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createLoginUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accessoriesSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unitSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem specificationSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderDashboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createBuyerOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeBuyerOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accessoriesRequirementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receivingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deliveryToProductionToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem transferToOtherStyleToolStripMenuItem;
    }
}