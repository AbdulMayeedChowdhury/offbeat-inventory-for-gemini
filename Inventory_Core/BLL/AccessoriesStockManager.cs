﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class AccessoriesStockManager
    {
        private OffbeatInventoryEntities _entities;

        public AccessoriesStockManager(OffbeatInventoryEntities entities)
        {
            _entities = entities;
        }

        public void IncreaseStock(string accessoriesId, int attributeId, string size, int mUnitId, int areaCode, decimal quantity)
        {
            AccessoriesStock stock = _entities.AccessoriesStocks.SingleOrDefault(s => s.AccessoriesId == accessoriesId && s.AttributeId == attributeId && 
                                                                s.Size == size && s.MUnitId == mUnitId && s.AreaCode == areaCode);

            if (stock == null)
            {
                stock = new AccessoriesStock { AccessoriesId = accessoriesId, AttributeId = attributeId, Size = size,
                                                MUnitId = mUnitId, AreaCode = areaCode, CurrentStock = quantity };

                _entities.AddToAccessoriesStocks(stock);
            }
            else
            {
                stock.CurrentStock += quantity;
            }
        }

        public void DecreaseStock(string accessoriesId, int attributeId, string size, int mUnitId, int areaCode, decimal quantity)
        {
            AccessoriesStock stock = _entities.AccessoriesStocks.SingleOrDefault(s => s.AccessoriesId == accessoriesId && s.AttributeId == attributeId &&
                                                                s.Size == size && s.MUnitId == mUnitId && s.AreaCode == areaCode);

            if (stock == null)
            {
                throw new Exception("No stock found");
            }
            else
            {
                if (stock.CurrentStock - quantity < 0)
                {
                    throw new Exception("Not enough stock");
                }
                
                stock.CurrentStock -= quantity;
            }
        }
    }
}
