﻿namespace Inventory_GUI
{
    partial class TransferAccessoriesToOtherOrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.transferIdLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.transferDataGridView = new System.Windows.Forms.DataGridView();
            this.referenceTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.transferDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.clearButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.fromStyleTextBox = new System.Windows.Forms.TextBox();
            this.toStyleTextBox = new System.Windows.Forms.TextBox();
            this.loadButton = new System.Windows.Forms.Button();
            this.fromOrderLabel = new System.Windows.Forms.Label();
            this.toOrderLabel = new System.Windows.Forms.Label();
            this.AccessoriesId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccessoriesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Detail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SpecId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SpecName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MeasurementId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransferQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transferDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(273, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 53;
            this.label4.Text = "Transfer Date";
            // 
            // transferIdLabel
            // 
            this.transferIdLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.transferIdLabel.Location = new System.Drawing.Point(87, 22);
            this.transferIdLabel.Name = "transferIdLabel";
            this.transferIdLabel.Size = new System.Drawing.Size(151, 21);
            this.transferIdLabel.TabIndex = 52;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 51;
            this.label3.Text = "Transfer Id";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.transferDataGridView);
            this.groupBox2.Location = new System.Drawing.Point(14, 148);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(741, 362);
            this.groupBox2.TabIndex = 50;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Transfer Information";
            // 
            // transferDataGridView
            // 
            this.transferDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.transferDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AccessoriesId,
            this.AccessoriesName,
            this.Detail,
            this.SpecId,
            this.SpecName,
            this.Size,
            this.MeasurementId,
            this.Measurement,
            this.CurrentBalance,
            this.TransferQuantity});
            this.transferDataGridView.Location = new System.Drawing.Point(9, 27);
            this.transferDataGridView.Name = "transferDataGridView";
            this.transferDataGridView.Size = new System.Drawing.Size(719, 326);
            this.transferDataGridView.TabIndex = 31;
            // 
            // referenceTextBox
            // 
            this.referenceTextBox.Location = new System.Drawing.Point(89, 53);
            this.referenceTextBox.Name = "referenceTextBox";
            this.referenceTextBox.Size = new System.Drawing.Size(495, 20);
            this.referenceTextBox.TabIndex = 59;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 58;
            this.label10.Text = "Reference";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(19, 79);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 60;
            this.label11.Text = "From Style";
            // 
            // transferDateTimePicker
            // 
            this.transferDateTimePicker.Location = new System.Drawing.Point(366, 22);
            this.transferDateTimePicker.Name = "transferDateTimePicker";
            this.transferDateTimePicker.Size = new System.Drawing.Size(218, 20);
            this.transferDateTimePicker.TabIndex = 54;
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(651, 53);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(91, 33);
            this.clearButton.TabIndex = 57;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(651, 88);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(91, 33);
            this.exitButton.TabIndex = 56;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(651, 18);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(91, 33);
            this.saveButton.TabIndex = 55;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(273, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 62;
            this.label1.Text = "To Style";
            // 
            // fromStyleTextBox
            // 
            this.fromStyleTextBox.Location = new System.Drawing.Point(89, 80);
            this.fromStyleTextBox.Name = "fromStyleTextBox";
            this.fromStyleTextBox.Size = new System.Drawing.Size(107, 20);
            this.fromStyleTextBox.TabIndex = 63;
            this.fromStyleTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.fromStyleTextBox_Validating);
            // 
            // toStyleTextBox
            // 
            this.toStyleTextBox.Location = new System.Drawing.Point(366, 79);
            this.toStyleTextBox.Name = "toStyleTextBox";
            this.toStyleTextBox.Size = new System.Drawing.Size(110, 20);
            this.toStyleTextBox.TabIndex = 64;
            this.toStyleTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.toStyleTextBox_Validating);
            // 
            // loadButton
            // 
            this.loadButton.Location = new System.Drawing.Point(493, 79);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(91, 52);
            this.loadButton.TabIndex = 65;
            this.loadButton.Text = "Load";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // fromOrderLabel
            // 
            this.fromOrderLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fromOrderLabel.Location = new System.Drawing.Point(88, 108);
            this.fromOrderLabel.Name = "fromOrderLabel";
            this.fromOrderLabel.Size = new System.Drawing.Size(108, 21);
            this.fromOrderLabel.TabIndex = 67;
            // 
            // toOrderLabel
            // 
            this.toOrderLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toOrderLabel.Location = new System.Drawing.Point(366, 108);
            this.toOrderLabel.Name = "toOrderLabel";
            this.toOrderLabel.Size = new System.Drawing.Size(110, 21);
            this.toOrderLabel.TabIndex = 68;
            // 
            // AccessoriesId
            // 
            this.AccessoriesId.DataPropertyName = "AccessoriesId";
            this.AccessoriesId.HeaderText = "Accessories Id";
            this.AccessoriesId.Name = "AccessoriesId";
            this.AccessoriesId.ReadOnly = true;
            this.AccessoriesId.Visible = false;
            // 
            // AccessoriesName
            // 
            this.AccessoriesName.DataPropertyName = "AccessoriesName";
            this.AccessoriesName.HeaderText = "Accessories Name";
            this.AccessoriesName.Name = "AccessoriesName";
            this.AccessoriesName.ReadOnly = true;
            // 
            // Detail
            // 
            this.Detail.DataPropertyName = "Detail";
            this.Detail.HeaderText = "Detail";
            this.Detail.Name = "Detail";
            this.Detail.ReadOnly = true;
            // 
            // SpecId
            // 
            this.SpecId.DataPropertyName = "AttributeId";
            this.SpecId.HeaderText = "Spec Id";
            this.SpecId.Name = "SpecId";
            this.SpecId.ReadOnly = true;
            this.SpecId.Visible = false;
            // 
            // SpecName
            // 
            this.SpecName.DataPropertyName = "AttributeName";
            this.SpecName.HeaderText = "Spec/Color";
            this.SpecName.Name = "SpecName";
            this.SpecName.ReadOnly = true;
            // 
            // Size
            // 
            this.Size.DataPropertyName = "Size";
            this.Size.HeaderText = "Size";
            this.Size.Name = "Size";
            // 
            // MeasurementId
            // 
            this.MeasurementId.DataPropertyName = "MUnit";
            this.MeasurementId.HeaderText = "MUnit Id";
            this.MeasurementId.Name = "MeasurementId";
            this.MeasurementId.ReadOnly = true;
            this.MeasurementId.Visible = false;
            // 
            // Measurement
            // 
            this.Measurement.DataPropertyName = "MUnitName";
            this.Measurement.HeaderText = "Unit";
            this.Measurement.Name = "Measurement";
            this.Measurement.ReadOnly = true;
            // 
            // CurrentBalance
            // 
            this.CurrentBalance.DataPropertyName = "CurrentStock";
            this.CurrentBalance.HeaderText = "Current Balance";
            this.CurrentBalance.Name = "CurrentBalance";
            this.CurrentBalance.ReadOnly = true;
            // 
            // TransferQuantity
            // 
            this.TransferQuantity.HeaderText = "Transfer Quantity";
            this.TransferQuantity.Name = "TransferQuantity";
            // 
            // TransferAccessoriesToOtherOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 520);
            this.Controls.Add(this.toOrderLabel);
            this.Controls.Add(this.fromOrderLabel);
            this.Controls.Add(this.loadButton);
            this.Controls.Add(this.toStyleTextBox);
            this.Controls.Add(this.fromStyleTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.transferIdLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.referenceTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.transferDateTimePicker);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.saveButton);
            this.Name = "TransferAccessoriesToOtherOrderForm";
            this.Text = "TransferAccessoriesToOtherOrderForm";
            this.Load += new System.EventHandler(this.TransferAccessoriesToOtherOrderForm_Load);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.transferDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label transferIdLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView transferDataGridView;
        private System.Windows.Forms.TextBox referenceTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker transferDateTimePicker;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox fromStyleTextBox;
        private System.Windows.Forms.TextBox toStyleTextBox;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.Label fromOrderLabel;
        private System.Windows.Forms.Label toOrderLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccessoriesId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccessoriesName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Detail;
        private System.Windows.Forms.DataGridViewTextBoxColumn SpecId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SpecName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn MeasurementId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransferQuantity;
    }
}