﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Inventory_Core.DAL;
using Inventory_Core.BLL;
using System.Data.Objects.DataClasses;

namespace Inventory_GUI
{
    public partial class ReceivingForm : BaseForm
    {
        private string _styleNo = string.Empty;

        private class OrderDetail
        {
            public string ItemCode { get; set; }
            public string ItemName { get; set; }
            public int SpecId { get; set; }
            public string SpecName { get; set; }
            public string Size { get; set; }
            public int MUnitId { get; set; }
            public string MUnitName { get; set; }
            public decimal Quantity { get; set; }
        }

        #region Constructor

        public ReceivingForm()
        {
            InitializeComponent();
        }

        public ReceivingForm(string styleNo) : this()
        {
            _styleNo = styleNo;
        }

        #endregion

        #region Event Handler

        private void ReceivingForm_Load(object sender, EventArgs e)
        {
            ClearFields();

            if (_styleNo.Length > 0)
            {
                styleNoTextBox.Text = _styleNo;
                styleNoTextBox.Enabled = false;
                LoadStyleRelatedInformation(_styleNo);
            }
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void styleNoTextBox_Validating(object sender, CancelEventArgs e)
        {
            string styleNo = styleNoTextBox.Text.Trim();

            if (styleNo.Length == 0)
            {
                buyerLabel.Text = string.Empty;
                poComboBox.DataSource = null;

                orderDetailDataGridView.DataSource = null;
                receivingDataGridView.Rows.Clear();

                return;
            }

            LoadStyleRelatedInformation(styleNo);
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            if (_styleNo.Length == 0)
            {
                ClearFields(); 
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (styleNoTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show(@"Enter style number");
                styleNoTextBox.Focus();
                return;
            }

            if (receivingDataGridView.Rows.Count == 0)
            {
                MessageBox.Show(@"No data found to save.");
                styleNoTextBox.Focus();
                return;
            }

            var receiveDetails = new EntityCollection<AccessoriesReceivingDetail>();

            decimal totalReceiveQuantity = 0;

            for (int i = 0; i < receivingDataGridView.Rows.Count - 1; i++)
            {
                var receiveDetail = new AccessoriesReceivingDetail();

                receiveDetail.AccessoriesId = receivingDataGridView.Rows[i].Cells["AccessoriesId"].Value.ToString();
                receiveDetail.AttributeId = Convert.ToInt32(receivingDataGridView.Rows[i].Cells["SpecId"].Value);
                receiveDetail.Size = Convert.ToString(receivingDataGridView.Rows[i].Cells["Size"].Value);
                receiveDetail.MUnitId = Convert.ToInt32(receivingDataGridView.Rows[i].Cells["MeasurementId"].Value);

                if (Convert.ToString(receivingDataGridView.Rows[i].Cells["ReceiveQuantity"].Value) == string.Empty)
                {
                    receiveDetail.Quantity = 0;
                }
                else
                {
                    receiveDetail.Quantity = Convert.ToDecimal(receivingDataGridView.Rows[i].Cells["ReceiveQuantity"].Value);
                }

                totalReceiveQuantity += receiveDetail.Quantity;
                receiveDetails.Add(receiveDetail);
            }

            if (totalReceiveQuantity == 0)
            {
                MessageBox.Show(@"No receive quantity found to save.");
                return;
            }

            var newReceiving = new AccessoriesReceiving();

            newReceiving.ReceivingDate = receivingDateTimePicker.Value;
            newReceiving.POId = poComboBox.SelectedValue.ToString();

            if (referenceTextBox.Text.Trim().Length > 0)
            {
                newReceiving.ReferenceNo = referenceTextBox.Text.Trim();
            }
            
            newReceiving.EntryUserId = LoginInformation.User.UserId;
            newReceiving.EntryDate = DateTime.Now;
            newReceiving.AccessoriesReceivingDetails = receiveDetails;

            try
            {
                new ReceivingManager().SaveReceiving(newReceiving);

                receivingIdLabel.Text = newReceiving.ReceivingId;

                MessageBox.Show(@"New Receiving created successfully. Receiving Id: " + newReceiving.ReceivingId);
                
                ClearFields();
            }
            catch (Exception exception)
            {
                MessageBox.Show(@"Error: " + exception.Message);
                throw;
            }
        }

        #endregion

        #region Private Methods

        private void poComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (poComboBox.SelectedValue == null)
            {
                receivingDataGridView.Rows.Clear();
                return;
            }

            if (poComboBox.SelectedValue.ToString() == "select" )
            {
                receivingDataGridView.Rows.Clear();
                return;
            }

            AccessoriesPurchaseOrder po = new PurchaseOrderManager().GetPurchaseOrder(poComboBox.SelectedValue.ToString());

            if (po == null)
            {
                MessageBox.Show(@"No PO information found.");
                poComboBox.Focus();
                return;
            }

            if (!po.IsActive)
            {
                MessageBox.Show(@"Inactive PO.");
                poComboBox.Focus();
                return;
            }

            foreach (var detail in po.AccessoriesPurchaseOrderDetails)
            {
                receivingDataGridView.Rows.Add(detail.AccessoriesId, detail.Accessory.AccessoriesName,
                                        detail.AttributeId, detail.Attribute.AttributeName, detail.Size,
                                        detail.MUnitId, detail.MeasurementUnit.MUnitName,
                                        detail.Quantity, detail.PreviousTotalReceivedQuantity);
            }
        }

        private void ClearFields()
        {
            receivingIdLabel.Text = string.Empty;
            receivingDateTimePicker.Value = DateTime.Now;
            referenceTextBox.Text = string.Empty;

            styleNoTextBox.Text = string.Empty;
            buyerLabel.Text = string.Empty;
            poComboBox.DataSource = null;

            orderDetailDataGridView.DataSource = null;
            receivingDataGridView.Rows.Clear();
        }

        private void LoadStyleRelatedInformation(string styleNo)
        {
            BuyerOrder buyerOrder = new BuyerOrderManager().GetBuyerOrderByStyleNo(styleNo);

            if (buyerOrder == null)
            {
                buyerLabel.Text = string.Empty;
                poComboBox.DataSource = null;

                orderDetailDataGridView.DataSource = null;
                receivingDataGridView.Rows.Clear();

                return;
            }

            if (buyerOrder.IsClosed)
            {
                MessageBox.Show(@"This order is closed");
                buyerLabel.Text = string.Empty;
                poComboBox.DataSource = null;

                orderDetailDataGridView.DataSource = null;
                receivingDataGridView.Rows.Clear();

                return;
            }

            buyerLabel.Text = buyerOrder.Buyer.BuyerName;

            LoadPOCombobox(styleNo);

            var orderDetails = new List<OrderDetail>();

            foreach (var detail in buyerOrder.BuyerOrderDetails)
            {
                var orderDetail = new OrderDetail();

                orderDetail.ItemCode = detail.ItemCode;
                orderDetail.ItemName = detail.Item.ItemName;
                orderDetail.SpecId = detail.AttributeId;
                orderDetail.SpecName = detail.Attribute.AttributeName;
                orderDetail.Size = detail.Size;
                orderDetail.MUnitId = detail.MUnitId;
                orderDetail.MUnitName = detail.MeasurementUnit.MUnitName;
                orderDetail.Quantity = detail.Quantity;

                orderDetails.Add(orderDetail);
            }

            orderDetailDataGridView.AutoGenerateColumns = false;
            orderDetailDataGridView.DataSource = orderDetails;
        }

        private void LoadPOCombobox(string styleNo)
        {
            List<AccessoriesPurchaseOrder> purchaseOrders = new List<AccessoriesPurchaseOrder>();

            var po = new PurchaseOrderManager().GetPurchaseOrderByStyleNo(styleNo);

            if (po.IsActive)
            {
                purchaseOrders.Add(po);
            }

            purchaseOrders.Insert(0, new AccessoriesPurchaseOrder { POId = "select" });

            poComboBox.ValueMember = "POId";
            poComboBox.DisplayMember = "POId";

            poComboBox.DataSource = purchaseOrders;
        }

        #endregion
    }
}
