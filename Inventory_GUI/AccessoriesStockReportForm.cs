﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Inventory_Core.DAL;
using Inventory_Core.BLL;

namespace Inventory_GUI
{
    public partial class AccessoriesStockReportForm : Form
    {
        public AccessoriesStockReportForm()
        {
            InitializeComponent();
        }

        private void AccessoriesStockReportForm_Load(object sender, EventArgs e)
        {
            LoadAreaList();
        }

        private void LoadAreaList()
        {
            List<Area> areas = new AreaManager().GetAreas(true);

            areas.Insert(0, new Area { AreaCode = 0, AreaName = "--All Area--" });

            areaComboBox.ValueMember = "AreaCode";
            areaComboBox.DisplayMember = "AreaName";

            areaComboBox.DataSource = areas;
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            this.AreaWiseAccessoriesStockTableAdapter.Fill(this.OffbeatInventoryDataSet.AreaWiseAccessoriesStock, (int)areaComboBox.SelectedValue);

            this.reportViewer1.RefreshReport();
        }
    }
}
