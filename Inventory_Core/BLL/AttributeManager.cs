﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class AttributeManager
    {
        private OffbeatInventoryEntities _entities;

        public AttributeManager()
        {
            _entities = new OffbeatInventoryEntities();    
        }

        public List<Inventory_Core.DAL.Attribute> GetAttributes(bool isActiveOnly)
        {
            var attributes = new List<Inventory_Core.DAL.Attribute>();

            if (isActiveOnly)
            {
                attributes = _entities.Attributes.Where(a => a.IsActive == true).ToList();
            }
            else
            {
                attributes = _entities.Attributes.ToList();
            }

            return attributes;
        }

        public Inventory_Core.DAL.Attribute GetAttribute(int attributeId)
        {
            var attribute = _entities.Attributes.FirstOrDefault(a => a.AttributeId == attributeId);

            return attribute;
        }

        public void SaveAttribute(Inventory_Core.DAL.Attribute attribute)
        {
            try
            {
                using (_entities)
                {
                    _entities.AddToAttributes(attribute);
                    _entities.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }
    }
}
