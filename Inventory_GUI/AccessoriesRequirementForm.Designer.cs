﻿namespace Inventory_GUI
{
    partial class AccessoriesRequirementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.orderIdLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.orderDetailDataGridView = new System.Windows.Forms.DataGridView();
            this.styleNoTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buyerLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.accessoriesGroupBox = new System.Windows.Forms.GroupBox();
            this.sizeTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.accessoriesCategoryComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.detailTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.measurementComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.accessoriesRequiredDataGridView = new System.Windows.Forms.DataGridView();
            this.AccessoriesId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccessoriesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Detail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttributeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttributeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addToGridButton = new System.Windows.Forms.Button();
            this.quantityTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.specificationComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.accessoriesComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemAttributeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemAttributeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemMeasurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailDataGridView)).BeginInit();
            this.accessoriesGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.accessoriesRequiredDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.orderIdLabel);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.orderDetailDataGridView);
            this.groupBox1.Controls.Add(this.styleNoTextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.buyerLabel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(831, 139);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Order Information";
            // 
            // orderIdLabel
            // 
            this.orderIdLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.orderIdLabel.Location = new System.Drawing.Point(69, 81);
            this.orderIdLabel.Name = "orderIdLabel";
            this.orderIdLabel.Size = new System.Drawing.Size(224, 21);
            this.orderIdLabel.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 81);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Order Id";
            // 
            // orderDetailDataGridView
            // 
            this.orderDetailDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderDetailDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.ItemAttributeId,
            this.ItemAttributeName,
            this.ItemSize,
            this.ItemMeasurement,
            this.ItemQuantity});
            this.orderDetailDataGridView.Location = new System.Drawing.Point(310, 19);
            this.orderDetailDataGridView.Name = "orderDetailDataGridView";
            this.orderDetailDataGridView.Size = new System.Drawing.Size(515, 105);
            this.orderDetailDataGridView.TabIndex = 14;
            // 
            // styleNoTextBox
            // 
            this.styleNoTextBox.Location = new System.Drawing.Point(69, 28);
            this.styleNoTextBox.Name = "styleNoTextBox";
            this.styleNoTextBox.Size = new System.Drawing.Size(100, 20);
            this.styleNoTextBox.TabIndex = 13;
            this.styleNoTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.styleNoTextBox_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Style No.";
            // 
            // buyerLabel
            // 
            this.buyerLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.buyerLabel.Location = new System.Drawing.Point(69, 54);
            this.buyerLabel.Name = "buyerLabel";
            this.buyerLabel.Size = new System.Drawing.Size(224, 21);
            this.buyerLabel.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Buyer";
            // 
            // accessoriesGroupBox
            // 
            this.accessoriesGroupBox.Controls.Add(this.sizeTextBox);
            this.accessoriesGroupBox.Controls.Add(this.label10);
            this.accessoriesGroupBox.Controls.Add(this.accessoriesCategoryComboBox);
            this.accessoriesGroupBox.Controls.Add(this.label9);
            this.accessoriesGroupBox.Controls.Add(this.detailTextBox);
            this.accessoriesGroupBox.Controls.Add(this.label7);
            this.accessoriesGroupBox.Controls.Add(this.measurementComboBox);
            this.accessoriesGroupBox.Controls.Add(this.label2);
            this.accessoriesGroupBox.Controls.Add(this.accessoriesRequiredDataGridView);
            this.accessoriesGroupBox.Controls.Add(this.addToGridButton);
            this.accessoriesGroupBox.Controls.Add(this.quantityTextBox);
            this.accessoriesGroupBox.Controls.Add(this.label6);
            this.accessoriesGroupBox.Controls.Add(this.specificationComboBox);
            this.accessoriesGroupBox.Controls.Add(this.label4);
            this.accessoriesGroupBox.Controls.Add(this.accessoriesComboBox);
            this.accessoriesGroupBox.Controls.Add(this.label3);
            this.accessoriesGroupBox.Location = new System.Drawing.Point(12, 156);
            this.accessoriesGroupBox.Name = "accessoriesGroupBox";
            this.accessoriesGroupBox.Size = new System.Drawing.Size(941, 414);
            this.accessoriesGroupBox.TabIndex = 13;
            this.accessoriesGroupBox.TabStop = false;
            this.accessoriesGroupBox.Text = "Accessories Required";
            // 
            // sizeTextBox
            // 
            this.sizeTextBox.Location = new System.Drawing.Point(311, 56);
            this.sizeTextBox.Name = "sizeTextBox";
            this.sizeTextBox.Size = new System.Drawing.Size(115, 20);
            this.sizeTextBox.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(277, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "Size";
            // 
            // accessoriesCategoryComboBox
            // 
            this.accessoriesCategoryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.accessoriesCategoryComboBox.FormattingEnabled = true;
            this.accessoriesCategoryComboBox.Location = new System.Drawing.Point(92, 26);
            this.accessoriesCategoryComboBox.Name = "accessoriesCategoryComboBox";
            this.accessoriesCategoryComboBox.Size = new System.Drawing.Size(134, 21);
            this.accessoriesCategoryComboBox.TabIndex = 26;
            this.accessoriesCategoryComboBox.SelectedIndexChanged += new System.EventHandler(this.accessoriesCategoryComboBox_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Category";
            // 
            // detailTextBox
            // 
            this.detailTextBox.Location = new System.Drawing.Point(649, 29);
            this.detailTextBox.Name = "detailTextBox";
            this.detailTextBox.Size = new System.Drawing.Size(259, 20);
            this.detailTextBox.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(571, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Detail";
            // 
            // measurementComboBox
            // 
            this.measurementComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.measurementComboBox.FormattingEnabled = true;
            this.measurementComboBox.Location = new System.Drawing.Point(529, 56);
            this.measurementComboBox.Name = "measurementComboBox";
            this.measurementComboBox.Size = new System.Drawing.Size(114, 21);
            this.measurementComboBox.TabIndex = 22;
            this.measurementComboBox.SelectedIndexChanged += new System.EventHandler(this.measurementComboBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(469, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Unit";
            // 
            // accessoriesRequiredDataGridView
            // 
            this.accessoriesRequiredDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.accessoriesRequiredDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AccessoriesId,
            this.AccessoriesName,
            this.Detail,
            this.AttributeId,
            this.AttributeName,
            this.Size,
            this.UnitId,
            this.Measurement,
            this.Quantity});
            this.accessoriesRequiredDataGridView.Location = new System.Drawing.Point(15, 104);
            this.accessoriesRequiredDataGridView.Name = "accessoriesRequiredDataGridView";
            this.accessoriesRequiredDataGridView.Size = new System.Drawing.Size(893, 300);
            this.accessoriesRequiredDataGridView.TabIndex = 20;
            // 
            // AccessoriesId
            // 
            this.AccessoriesId.HeaderText = "Accessories Id";
            this.AccessoriesId.Name = "AccessoriesId";
            this.AccessoriesId.ReadOnly = true;
            this.AccessoriesId.Visible = false;
            // 
            // AccessoriesName
            // 
            this.AccessoriesName.HeaderText = "Accessories Name";
            this.AccessoriesName.Name = "AccessoriesName";
            this.AccessoriesName.ReadOnly = true;
            // 
            // Detail
            // 
            this.Detail.HeaderText = "Detail";
            this.Detail.Name = "Detail";
            // 
            // AttributeId
            // 
            this.AttributeId.HeaderText = "Spec Id";
            this.AttributeId.Name = "AttributeId";
            this.AttributeId.ReadOnly = true;
            this.AttributeId.Visible = false;
            // 
            // AttributeName
            // 
            this.AttributeName.HeaderText = "Spec/Color";
            this.AttributeName.Name = "AttributeName";
            this.AttributeName.ReadOnly = true;
            // 
            // Size
            // 
            this.Size.HeaderText = "Size";
            this.Size.Name = "Size";
            // 
            // UnitId
            // 
            this.UnitId.HeaderText = "Unit ID";
            this.UnitId.Name = "UnitId";
            // 
            // Measurement
            // 
            this.Measurement.HeaderText = "Unit";
            this.Measurement.Name = "Measurement";
            this.Measurement.ReadOnly = true;
            this.Measurement.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Measurement.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            this.Quantity.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Quantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // addToGridButton
            // 
            this.addToGridButton.Location = new System.Drawing.Point(821, 59);
            this.addToGridButton.Name = "addToGridButton";
            this.addToGridButton.Size = new System.Drawing.Size(91, 27);
            this.addToGridButton.TabIndex = 19;
            this.addToGridButton.Text = "Add To Grid";
            this.addToGridButton.UseVisualStyleBackColor = true;
            this.addToGridButton.Click += new System.EventHandler(this.addToGridButton_Click);
            // 
            // quantityTextBox
            // 
            this.quantityTextBox.Location = new System.Drawing.Point(730, 59);
            this.quantityTextBox.Name = "quantityTextBox";
            this.quantityTextBox.Size = new System.Drawing.Size(85, 20);
            this.quantityTextBox.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(676, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Quantity";
            // 
            // specificationComboBox
            // 
            this.specificationComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.specificationComboBox.FormattingEnabled = true;
            this.specificationComboBox.Location = new System.Drawing.Point(92, 56);
            this.specificationComboBox.Name = "specificationComboBox";
            this.specificationComboBox.Size = new System.Drawing.Size(134, 21);
            this.specificationComboBox.TabIndex = 16;
            this.specificationComboBox.SelectedIndexChanged += new System.EventHandler(this.specificationComboBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Specification";
            // 
            // accessoriesComboBox
            // 
            this.accessoriesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.accessoriesComboBox.FormattingEnabled = true;
            this.accessoriesComboBox.Location = new System.Drawing.Point(355, 26);
            this.accessoriesComboBox.Name = "accessoriesComboBox";
            this.accessoriesComboBox.Size = new System.Drawing.Size(174, 21);
            this.accessoriesComboBox.TabIndex = 14;
            this.accessoriesComboBox.SelectedIndexChanged += new System.EventHandler(this.accessoriesComboBox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(277, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Accessories";
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(858, 54);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(91, 33);
            this.clearButton.TabIndex = 19;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(858, 89);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(91, 33);
            this.exitButton.TabIndex = 18;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(858, 19);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(91, 33);
            this.saveButton.TabIndex = 17;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // ItemCode
            // 
            this.ItemCode.DataPropertyName = "ItemCode";
            this.ItemCode.HeaderText = "Code";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.ReadOnly = true;
            this.ItemCode.Visible = false;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // ItemAttributeId
            // 
            this.ItemAttributeId.DataPropertyName = "SpecId";
            this.ItemAttributeId.HeaderText = "Spec. Id";
            this.ItemAttributeId.Name = "ItemAttributeId";
            this.ItemAttributeId.ReadOnly = true;
            this.ItemAttributeId.Visible = false;
            // 
            // ItemAttributeName
            // 
            this.ItemAttributeName.DataPropertyName = "SpecName";
            this.ItemAttributeName.HeaderText = "Spec/Color";
            this.ItemAttributeName.Name = "ItemAttributeName";
            this.ItemAttributeName.ReadOnly = true;
            // 
            // ItemSize
            // 
            this.ItemSize.DataPropertyName = "Size";
            this.ItemSize.HeaderText = "Size";
            this.ItemSize.Name = "ItemSize";
            // 
            // ItemMeasurement
            // 
            this.ItemMeasurement.DataPropertyName = "MUnitName";
            this.ItemMeasurement.HeaderText = "Unit";
            this.ItemMeasurement.Name = "ItemMeasurement";
            this.ItemMeasurement.ReadOnly = true;
            // 
            // ItemQuantity
            // 
            this.ItemQuantity.DataPropertyName = "Quantity";
            this.ItemQuantity.HeaderText = "Quantity";
            this.ItemQuantity.Name = "ItemQuantity";
            this.ItemQuantity.ReadOnly = true;
            // 
            // AccessoriesRequirementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(965, 584);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.accessoriesGroupBox);
            this.Controls.Add(this.groupBox1);
            this.Name = "AccessoriesRequirementForm";
            this.Text = "Accessories Requirement";
            this.Load += new System.EventHandler(this.AccessoriesRequirementForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailDataGridView)).EndInit();
            this.accessoriesGroupBox.ResumeLayout(false);
            this.accessoriesGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.accessoriesRequiredDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox styleNoTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label buyerLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView orderDetailDataGridView;
        private System.Windows.Forms.GroupBox accessoriesGroupBox;
        private System.Windows.Forms.DataGridView accessoriesRequiredDataGridView;
        private System.Windows.Forms.Button addToGridButton;
        private System.Windows.Forms.TextBox quantityTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox specificationComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox accessoriesComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.ComboBox measurementComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label orderIdLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox detailTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox accessoriesCategoryComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox sizeTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccessoriesId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccessoriesName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Detail;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttributeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttributeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemAttributeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemAttributeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemMeasurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemQuantity;
    }
}