﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Inventory_Core.BLL;
using Inventory_Core.DAL;

namespace Inventory_GUI
{
    public partial class LoginUserSetup : BaseForm
    {
        private LoginUserManager _loginUserManager;

        public LoginUserSetup()
        {
            InitializeComponent();
        }

        private void saveButton_Click(object sender, System.EventArgs e)
        {
            string userId = userIdTextBox.Text.Trim();

            if (userId.Length == 0)
            {
                MessageBox.Show(@"User Id required");
                return;
            }

            string fullName = fullNameTextBox.Text.Trim();

            if (fullName.Length == 0)
            {
                MessageBox.Show(@"Full name required");
                return;
            }

            string password = passwordTextBox.Text;

            if (password.Length == 0)
            {
                MessageBox.Show(@"Password required");
                return;
            }

            if (password != retypePasswordTextBox.Text )
            {
                MessageBox.Show(@"Password does not match with retype password.");
                return;
            }

            LoginUser user = new LoginUser();

            user.UserId = userIdTextBox.Text.Trim();
            user.FullName = fullName;
            user.Password = password;

            try
            {
                _loginUserManager.SaveLoginUser(user);
            }
            catch (Exception exception)
            {
                MessageBox.Show(@"Failed to save" + Environment.NewLine + @"Error: " + exception.Message);
            }
            
            LoadLoginUsers();
        }

        private void LoginUserSetup_Load(object sender, System.EventArgs e)
        {
            _loginUserManager = new LoginUserManager();

            LoadLoginUsers();
        }

        private void LoadLoginUsers()
        {
            List<LoginUser> loginUsers = _loginUserManager.GetLoginUsers();

            loginUserDataGridView.AutoGenerateColumns = false;
            loginUserDataGridView.DataSource = loginUsers;
        }

        private void loginUserDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string userId = loginUserDataGridView.Rows[e.RowIndex].Cells["UserId"].ToString();

            LoginUser user = _loginUserManager.GetLoginUser(userId);

            if (user!=null)
            {
                userIdTextBox.Text = user.UserId;
                fullNameTextBox.Text = user.FullName;
                passwordTextBox.Text = user.Password;
            }
        }

        private void cancelButton_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void userIdTextBox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string userId = userIdTextBox.Text.Trim();

            if (userId.Length == 0)
            {
                return;
            }

            LoginUser user = _loginUserManager.GetLoginUser(userId);

            if (user != null)
            {
                userIdTextBox.Text = user.UserId;
                fullNameTextBox.Text = user.FullName;
            }
        }
    }
}
