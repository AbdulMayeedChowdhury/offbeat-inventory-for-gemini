﻿namespace Inventory_GUI
{
    partial class ReceivingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clearButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.styleNoTextBox = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.receivingDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.receivingIdLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buyerLabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.receivingDataGridView = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.orderDetailDataGridView = new System.Windows.Forms.DataGridView();
            this.poComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.referenceTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemAttributeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemAttributeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemMeasurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccessoriesId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccessoriesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SpecId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SpecName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MeasurementId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderedQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlreadyReceived = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceiveQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.receivingDataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(862, 58);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(91, 33);
            this.clearButton.TabIndex = 31;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(862, 102);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(91, 33);
            this.exitButton.TabIndex = 30;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // styleNoTextBox
            // 
            this.styleNoTextBox.Location = new System.Drawing.Point(61, 29);
            this.styleNoTextBox.Name = "styleNoTextBox";
            this.styleNoTextBox.Size = new System.Drawing.Size(100, 20);
            this.styleNoTextBox.TabIndex = 17;
            this.styleNoTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.styleNoTextBox_Validating);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(862, 19);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(91, 33);
            this.saveButton.TabIndex = 29;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // receivingDateTimePicker
            // 
            this.receivingDateTimePicker.Location = new System.Drawing.Point(535, 10);
            this.receivingDateTimePicker.Name = "receivingDateTimePicker";
            this.receivingDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.receivingDateTimePicker.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Style No.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(431, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Receiving Date";
            // 
            // receivingIdLabel
            // 
            this.receivingIdLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.receivingIdLabel.Location = new System.Drawing.Point(98, 10);
            this.receivingIdLabel.Name = "receivingIdLabel";
            this.receivingIdLabel.Size = new System.Drawing.Size(151, 21);
            this.receivingIdLabel.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Receiving Id";
            // 
            // buyerLabel
            // 
            this.buyerLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.buyerLabel.Location = new System.Drawing.Point(61, 55);
            this.buyerLabel.Name = "buyerLabel";
            this.buyerLabel.Size = new System.Drawing.Size(264, 20);
            this.buyerLabel.TabIndex = 15;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.receivingDataGridView);
            this.groupBox2.Location = new System.Drawing.Point(12, 187);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(941, 327);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Receiving Information";
            // 
            // receivingDataGridView
            // 
            this.receivingDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.receivingDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AccessoriesId,
            this.AccessoriesName,
            this.SpecId,
            this.SpecName,
            this.Size,
            this.MeasurementId,
            this.Measurement,
            this.OrderedQuantity,
            this.AlreadyReceived,
            this.ReceiveQuantity});
            this.receivingDataGridView.Location = new System.Drawing.Point(12, 24);
            this.receivingDataGridView.Name = "receivingDataGridView";
            this.receivingDataGridView.Size = new System.Drawing.Size(905, 287);
            this.receivingDataGridView.TabIndex = 30;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Buyer";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.orderDetailDataGridView);
            this.groupBox1.Controls.Add(this.poComboBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.styleNoTextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.buyerLabel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(844, 119);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Buyer Order Information";
            // 
            // orderDetailDataGridView
            // 
            this.orderDetailDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderDetailDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.ItemAttributeId,
            this.ItemAttributeName,
            this.ItemSize,
            this.ItemMeasurement,
            this.ItemQuantity});
            this.orderDetailDataGridView.Location = new System.Drawing.Point(332, 18);
            this.orderDetailDataGridView.Name = "orderDetailDataGridView";
            this.orderDetailDataGridView.Size = new System.Drawing.Size(506, 92);
            this.orderDetailDataGridView.TabIndex = 22;
            // 
            // poComboBox
            // 
            this.poComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.poComboBox.FormattingEnabled = true;
            this.poComboBox.Location = new System.Drawing.Point(61, 82);
            this.poComboBox.Name = "poComboBox";
            this.poComboBox.Size = new System.Drawing.Size(182, 21);
            this.poComboBox.TabIndex = 20;
            this.poComboBox.SelectedIndexChanged += new System.EventHandler(this.poComboBox_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "PO";
            // 
            // referenceTextBox
            // 
            this.referenceTextBox.Location = new System.Drawing.Point(100, 39);
            this.referenceTextBox.Name = "referenceTextBox";
            this.referenceTextBox.Size = new System.Drawing.Size(264, 20);
            this.referenceTextBox.TabIndex = 33;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(30, 39);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Reference";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Enabled = false;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(535, 41);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(200, 21);
            this.comboBox1.TabIndex = 35;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(434, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 13);
            this.label11.TabIndex = 34;
            this.label11.Text = "Area";
            // 
            // ItemCode
            // 
            this.ItemCode.DataPropertyName = "ItemCode";
            this.ItemCode.HeaderText = "Code";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.ReadOnly = true;
            this.ItemCode.Visible = false;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // ItemAttributeId
            // 
            this.ItemAttributeId.DataPropertyName = "SpecId";
            this.ItemAttributeId.HeaderText = "Spec. Id";
            this.ItemAttributeId.Name = "ItemAttributeId";
            this.ItemAttributeId.ReadOnly = true;
            this.ItemAttributeId.Visible = false;
            // 
            // ItemAttributeName
            // 
            this.ItemAttributeName.DataPropertyName = "SpecName";
            this.ItemAttributeName.HeaderText = "Spec.";
            this.ItemAttributeName.Name = "ItemAttributeName";
            this.ItemAttributeName.ReadOnly = true;
            // 
            // ItemSize
            // 
            this.ItemSize.DataPropertyName = "Size";
            this.ItemSize.HeaderText = "Size";
            this.ItemSize.Name = "ItemSize";
            // 
            // ItemMeasurement
            // 
            this.ItemMeasurement.DataPropertyName = "MUnitName";
            this.ItemMeasurement.HeaderText = "Measurement";
            this.ItemMeasurement.Name = "ItemMeasurement";
            this.ItemMeasurement.ReadOnly = true;
            // 
            // ItemQuantity
            // 
            this.ItemQuantity.DataPropertyName = "Quantity";
            this.ItemQuantity.HeaderText = "Quantity";
            this.ItemQuantity.Name = "ItemQuantity";
            this.ItemQuantity.ReadOnly = true;
            // 
            // AccessoriesId
            // 
            this.AccessoriesId.HeaderText = "Accessories Id";
            this.AccessoriesId.Name = "AccessoriesId";
            this.AccessoriesId.ReadOnly = true;
            this.AccessoriesId.Visible = false;
            // 
            // AccessoriesName
            // 
            this.AccessoriesName.HeaderText = "Accessories Name";
            this.AccessoriesName.Name = "AccessoriesName";
            this.AccessoriesName.ReadOnly = true;
            // 
            // SpecId
            // 
            this.SpecId.HeaderText = "Spec Id";
            this.SpecId.Name = "SpecId";
            this.SpecId.ReadOnly = true;
            this.SpecId.Visible = false;
            // 
            // SpecName
            // 
            this.SpecName.HeaderText = "Spec/Color";
            this.SpecName.Name = "SpecName";
            this.SpecName.ReadOnly = true;
            // 
            // Size
            // 
            this.Size.HeaderText = "Size";
            this.Size.Name = "Size";
            // 
            // MeasurementId
            // 
            this.MeasurementId.HeaderText = "MUnit Id";
            this.MeasurementId.Name = "MeasurementId";
            this.MeasurementId.ReadOnly = true;
            this.MeasurementId.Visible = false;
            // 
            // Measurement
            // 
            this.Measurement.HeaderText = "Unit";
            this.Measurement.Name = "Measurement";
            this.Measurement.ReadOnly = true;
            // 
            // OrderedQuantity
            // 
            this.OrderedQuantity.HeaderText = "Ordered Quantity";
            this.OrderedQuantity.Name = "OrderedQuantity";
            this.OrderedQuantity.ReadOnly = true;
            // 
            // AlreadyReceived
            // 
            this.AlreadyReceived.HeaderText = "Already Received";
            this.AlreadyReceived.Name = "AlreadyReceived";
            // 
            // ReceiveQuantity
            // 
            this.ReceiveQuantity.HeaderText = "Receive Quantity";
            this.ReceiveQuantity.Name = "ReceiveQuantity";
            // 
            // ReceivingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(965, 521);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.referenceTextBox);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.receivingDateTimePicker);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.receivingIdLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ReceivingForm";
            this.Text = "ReceivingForm";
            this.Load += new System.EventHandler(this.ReceivingForm_Load);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.receivingDataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.TextBox styleNoTextBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.DateTimePicker receivingDateTimePicker;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label receivingIdLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label buyerLabel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox referenceTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox poComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView orderDetailDataGridView;
        private System.Windows.Forms.DataGridView receivingDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccessoriesId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccessoriesName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SpecId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SpecName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn MeasurementId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderedQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlreadyReceived;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceiveQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemAttributeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemAttributeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemMeasurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemQuantity;
    }
}