﻿namespace Inventory_GUI
{
    partial class SpecificationSetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.specificationDataGridView = new System.Windows.Forms.DataGridView();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.specificationNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AttributeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AttributeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.specificationDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // specificationDataGridView
            // 
            this.specificationDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.specificationDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AttributeId,
            this.AttributeName});
            this.specificationDataGridView.Location = new System.Drawing.Point(12, 85);
            this.specificationDataGridView.Name = "specificationDataGridView";
            this.specificationDataGridView.Size = new System.Drawing.Size(358, 244);
            this.specificationDataGridView.TabIndex = 22;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(295, 42);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 21;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(216, 42);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 20;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // specificationNameTextBox
            // 
            this.specificationNameTextBox.Location = new System.Drawing.Point(114, 16);
            this.specificationNameTextBox.Name = "specificationNameTextBox";
            this.specificationNameTextBox.Size = new System.Drawing.Size(257, 20);
            this.specificationNameTextBox.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Specification Name";
            // 
            // AttributeId
            // 
            this.AttributeId.DataPropertyName = "AttributeId";
            this.AttributeId.HeaderText = "Id";
            this.AttributeId.Name = "AttributeId";
            this.AttributeId.ReadOnly = true;
            // 
            // AttributeName
            // 
            this.AttributeName.DataPropertyName = "AttributeName";
            this.AttributeName.HeaderText = "Name";
            this.AttributeName.Name = "AttributeName";
            this.AttributeName.ReadOnly = true;
            // 
            // SpecificationSetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 346);
            this.Controls.Add(this.specificationDataGridView);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.specificationNameTextBox);
            this.Controls.Add(this.label1);
            this.Name = "SpecificationSetupForm";
            this.Text = "SpecificationSetupForm";
            this.Load += new System.EventHandler(this.SpecificationSetupForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.specificationDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView specificationDataGridView;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.TextBox specificationNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttributeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AttributeName;
    }
}