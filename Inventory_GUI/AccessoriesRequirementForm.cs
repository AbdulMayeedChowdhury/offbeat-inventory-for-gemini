﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Inventory_Core.DAL;
using Inventory_Core.BLL;
using System.Data.Objects.DataClasses;

namespace Inventory_GUI
{
    public partial class AccessoriesRequirementForm : BaseForm
    {
        private string _styleNo = string.Empty;

        private class OrderDetail
        {
            public string ItemCode { get; set; }
            public string ItemName { get; set; }
            public int SpecId { get; set; }
            public string SpecName { get; set; }
            public string Size { get; set; }
            public int MUnitId { get; set; }
            public string MUnitName { get; set; }
            public decimal Quantity { get; set; }
        }

        #region Constructor

        public AccessoriesRequirementForm()
        {
            InitializeComponent();
        }

        public AccessoriesRequirementForm(string styleNo) : this()
        {
            _styleNo = styleNo;
        }

        #endregion

        #region Event Handlers

        private void exitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AccessoriesRequirementForm_Load(object sender, EventArgs e)
        {
            LoadAccessoriesCategoryList();
            LoadAccessoriesList();
            LoadSpecificationList();
            LoadMeasurementList();
            //LoadDataGridViewMeasurementComboList();

            ClearFields();

            if (_styleNo.Length > 0)
            {
                styleNoTextBox.Text = _styleNo;
                LoadStyleRelatedInformation(_styleNo);
                styleNoTextBox.Enabled = false;
            }
        }

        private void styleNoTextBox_Validating(object sender, CancelEventArgs e)
        {
            string styleNo = styleNoTextBox.Text.Trim();

            if (styleNo.Length == 0)
            {
                buyerLabel.Text = string.Empty;
                orderIdLabel.Text = string.Empty;
                
                accessoriesGroupBox.Enabled = false;

                return;
            }

            LoadStyleRelatedInformation(styleNo);
        }

        private void quantityTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar) || (e.KeyChar == (char)Keys.Back) || (e.KeyChar == (char)Keys.Delete)))
                e.Handled = true;
            else
                e.Handled = false;
        }

        private void accessoriesCategoryComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadAccessoriesList();
        }

        private void accessoriesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (accessoriesComboBox.SelectedValue.ToString() == "select")
            {
                return;
            }

            if (accessoriesComboBox.SelectedValue.ToString() == "new")
            {
                var accessorySetupForm = new AccessorySetupForm();
                accessorySetupForm.ShowDialog();

                LoadAccessoriesList();
            }
        }

        private void specificationComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (specificationComboBox.SelectedValue.ToString() == "-1")
            {
                return;
            }

            if (specificationComboBox.SelectedValue.ToString() == "0")
            {
                var specificationSetupForm = new SpecificationSetupForm();
                specificationSetupForm.ShowDialog();

                LoadSpecificationList();
            }
        }

        private void measurementComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (measurementComboBox.SelectedValue.ToString() == "-1")
            {
                return;
            }

            if (measurementComboBox.SelectedValue.ToString() == "0")
            {
                var munitSetupForm = new MUnitSetupForm();
                munitSetupForm.ShowDialog();

                LoadMeasurementList();
                //LoadDataGridViewMeasurementComboList();
            }
        }

        private void addToGridButton_Click(object sender, EventArgs e)
        {
            if (styleNoTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show(@"Enter style number.");
                return;
            }
            
            if (accessoriesComboBox.SelectedValue.ToString() == "-1")
            {
                MessageBox.Show("Select accessory.");
                return;
            }

            //if (specificationComboBox.SelectedValue.ToString() == "-1")
            //{
            //    MessageBox.Show("Select specification.");
            //    return;
            //}

            if (measurementComboBox.SelectedValue.ToString() == "-1")
            {
                MessageBox.Show("Select Measurement.");
                return;
            }

            if (quantityTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show("Enter quantity.");
                return;
            }

            accessoriesRequiredDataGridView.Rows.Add(accessoriesComboBox.SelectedValue.ToString(), accessoriesComboBox.Text, detailTextBox.Text,
                                        specificationComboBox.SelectedValue.ToString() == "-1" ? "" : specificationComboBox.SelectedValue.ToString(),
                                        specificationComboBox.SelectedValue.ToString() == "-1" ? "" : specificationComboBox.Text, sizeTextBox.Text.Trim(),
                                        measurementComboBox.SelectedValue, measurementComboBox.SelectedText, Convert.ToDecimal(quantityTextBox.Text));
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (styleNoTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show(@"Enter style number");
                styleNoTextBox.Focus();
                return;
            }

            if (accessoriesRequiredDataGridView.Rows.Count == 0)
            {
                MessageBox.Show(@"No accessories found to save");
                return;
            }

            var accessoriesRequiredList = new EntityCollection<BuyerOrderAccessoriesRequired>();

            for (int i = 0; i < accessoriesRequiredDataGridView.Rows.Count - 1; i++)
            {
                var accessoriesRequired = new BuyerOrderAccessoriesRequired();

                accessoriesRequired.OrderId = orderIdLabel.Text;
                accessoriesRequired.AccessoriesId = Convert.ToString(accessoriesRequiredDataGridView.Rows[i].Cells["AccessoriesId"].Value);
                accessoriesRequired.Detail = Convert.ToString(accessoriesRequiredDataGridView.Rows[i].Cells["Detail"].Value);
                accessoriesRequired.AttributeId = Convert.ToInt32(accessoriesRequiredDataGridView.Rows[i].Cells["AttributeId"].Value);

                string size = Convert.ToString(accessoriesRequiredDataGridView.Rows[i].Cells["Size"].Value);
                accessoriesRequired.Size = (size == string.Empty ? "-" : size);
                
                accessoriesRequired.MUnitId = Convert.ToInt32(accessoriesRequiredDataGridView.Rows[i].Cells["UnitId"].Value); ; //(int)((DataGridViewComboBoxCell)accessoriesRequiredDataGridView.Rows[i].Cells["Measurement"]).Value
                accessoriesRequired.Quantity = Convert.ToDecimal(accessoriesRequiredDataGridView.Rows[i].Cells["Quantity"].Value);

                accessoriesRequiredList.Add(accessoriesRequired);
            }

            try
            {
                new AccessoriesRequirementManager().SaveAccessoriesRequired(accessoriesRequiredList);

                MessageBox.Show(@"Data saved successfully");

                ClearFields();
            }
            catch (Exception exception)
            {
                MessageBox.Show(@"Error: " + exception.Message);
                throw;
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            if (_styleNo.Length == 0)
            {
                ClearFields();
            }
        }

        #endregion

        #region Private Methods

        private void LoadAccessoriesCategoryList()
        {
            List<AccessoriesCategory> categories = new AccessoriesCategoryManager().GetAccessoriesCategories(true);

            categories.Insert(0, new AccessoriesCategory { CategoryId = 0, CategoryName = "--All Category--" });

            accessoriesCategoryComboBox.ValueMember = "CategoryId";
            accessoriesCategoryComboBox.DisplayMember = "CategoryName";

            accessoriesCategoryComboBox.DataSource = categories;

        }

        private void LoadAccessoriesList()
        {
            List<Accessory> accessories;

            int categoryId = Convert.ToInt32(accessoriesCategoryComboBox.SelectedValue);

            if (categoryId == 0)
            {
                accessories = new AccessoryManager().GetAccessories(true);
            }
            else
            {
                accessories = new AccessoryManager().GetAccessories(categoryId, true);
            }
            
            accessories.Insert(0, new Accessory { AccessoriesId = "select", AccessoriesName = "--Select Accessory--" });
            accessories.Insert(accessories.Count, new Accessory { AccessoriesId = "new", AccessoriesName = "--New Accessory--" });

            accessoriesComboBox.ValueMember = "AccessoriesId";
            accessoriesComboBox.DisplayMember = "AccessoriesName";

            accessoriesComboBox.DataSource = accessories;
        }

        public void LoadSpecificationList()
        {
            List<Inventory_Core.DAL.Attribute> attributes = new AttributeManager().GetAttributes(true);

            attributes.Insert(0, new Inventory_Core.DAL.Attribute { AttributeId = -1, AttributeName = "--Select Specification--" });
            attributes.Insert(attributes.Count, new Inventory_Core.DAL.Attribute { AttributeId = 0, AttributeName = "--New Attribute--" });

            specificationComboBox.ValueMember = "AttributeId";
            specificationComboBox.DisplayMember = "AttributeName";

            specificationComboBox.DataSource = attributes;
        }

        private void LoadMeasurementList()
        {
            List<MeasurementUnit> measurements = new MeasurementUnitManager().GetMeasurementUnits();

            measurements.Insert(0, new MeasurementUnit { MUnitId = -1, MUnitName = "--Select Measurement--" });
            measurements.Insert(measurements.Count, new MeasurementUnit { MUnitId = 0, MUnitName = "--New Measurement--" });

            measurementComboBox.ValueMember = "MUnitId";
            measurementComboBox.DisplayMember = "MUnitName";

            measurementComboBox.DataSource = measurements;
        }

        //private void LoadDataGridViewMeasurementComboList()
        //{
        //    DataGridViewComboBoxColumn comboColumn = (DataGridViewComboBoxColumn)accessoriesRequiredDataGridView.Columns["Measurement"];

        //    List<MeasurementUnit> measurements = new MeasurementUnitManager().GetMeasurementUnits();

        //    comboColumn.DisplayMember = "MUnitName";
        //    comboColumn.ValueMember = "MUnitId";
        //    comboColumn.DataSource = measurements;
        //}

        private void ClearFields()
        {
            styleNoTextBox.Text = string.Empty; 

            buyerLabel.Text = string.Empty;
            orderIdLabel.Text = string.Empty;

            orderDetailDataGridView.DataSource = null;
            accessoriesRequiredDataGridView.Rows.Clear();

            accessoriesCategoryComboBox.SelectedIndex = 0;

            LoadAccessoriesList();
            accessoriesComboBox.SelectedIndex = 0;
            
            specificationComboBox.SelectedIndex = 0;
            sizeTextBox.Text = string.Empty;
            measurementComboBox.SelectedIndex = 0;
            quantityTextBox.Text = string.Empty;

            accessoriesGroupBox.Enabled = false;

            styleNoTextBox.Focus();
        }

        private void LoadStyleRelatedInformation(string styleNo)
        {
            BuyerOrder buyerOrder = new BuyerOrderManager().GetBuyerOrderByStyleNo(styleNo);

            if (buyerOrder == null)
            {
                buyerLabel.Text = string.Empty;
                orderIdLabel.Text = string.Empty;

                accessoriesGroupBox.Enabled = false;

                return;
            }

            if (buyerOrder.IsClosed)
            {
                MessageBox.Show(@"This order is closed");
                buyerLabel.Text = string.Empty;
                orderIdLabel.Text = string.Empty;

                accessoriesGroupBox.Enabled = false;

                return;
            }

            buyerLabel.Text = buyerOrder.Buyer.BuyerName;
            orderIdLabel.Text = buyerOrder.OrderId;

            var orderDetails = new List<OrderDetail>();

            foreach (var detail in buyerOrder.BuyerOrderDetails)
            {
                var orderDetail = new OrderDetail();

                orderDetail.ItemCode = detail.ItemCode;
                orderDetail.ItemName = detail.Item.ItemName;
                orderDetail.SpecId = detail.AttributeId;
                orderDetail.SpecName = detail.Attribute.AttributeName;
                orderDetail.Size = detail.Size;
                orderDetail.MUnitId = detail.MUnitId;
                orderDetail.MUnitName = detail.MeasurementUnit.MUnitName;
                orderDetail.Quantity = detail.Quantity;

                orderDetails.Add(orderDetail);
            }

            orderDetailDataGridView.AutoGenerateColumns = false;
            orderDetailDataGridView.DataSource = orderDetails;

            accessoriesRequiredDataGridView.Rows.Clear();

            if (buyerOrder.BuyerOrderAccessoriesRequireds != null)
            {
                foreach (var accessoriesRequired in buyerOrder.BuyerOrderAccessoriesRequireds)
                {
                    accessoriesRequiredDataGridView.Rows.Add(accessoriesRequired.AccessoriesId,
                        accessoriesRequired.Accessory.AccessoriesName,
                        accessoriesRequired.Detail, accessoriesRequired.AttributeId, accessoriesRequired.Attribute.AttributeName,
                        accessoriesRequired.Size, accessoriesRequired.MUnitId, accessoriesRequired.MeasurementUnit.MUnitName, accessoriesRequired.Quantity);
                }
            }

            accessoriesGroupBox.Enabled = true;
        }

        #endregion

    }
}
