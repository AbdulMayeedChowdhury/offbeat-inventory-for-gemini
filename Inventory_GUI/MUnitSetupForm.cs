﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Inventory_Core.DAL;
using Inventory_Core.BLL;

namespace Inventory_GUI
{
    public partial class MUnitSetupForm : BaseForm
    {
        public MUnitSetupForm()
        {
            InitializeComponent();
        }

        private void MUnitSetupForm_Load(object sender, EventArgs e)
        {
            LoadMeasurements();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (measurementNameTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show("Enter measurement name");
                return;
            }

            var measurement = new MeasurementUnit { MUnitName = measurementNameTextBox.Text.Trim() };

            var measurementManager = new MeasurementUnitManager();

            try
            {
                measurementManager.SaveMeasurement(measurement);

                MessageBox.Show("Measurement saved successfully");
            }
            catch (Exception exception)
            {
                MessageBox.Show("Failed to save" + Environment.NewLine + "Error: " + exception.Message);
            }

            LoadMeasurements();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadMeasurements()
        {
            try
            {
                List<MeasurementUnit> measurements = new MeasurementUnitManager().GetMeasurementUnits();

                measurementDataGridView.AutoGenerateColumns = false;
                measurementDataGridView.DataSource = measurements;
            }
            catch (Exception exception)
            {
                MessageBox.Show("Failed to load measurements" + Environment.NewLine + "Error: " + exception.Message);
            }
        }


    }
}
