﻿using System;
using System.ComponentModel;
using System.Data.Objects.DataClasses;
using System.Windows.Forms;
using Inventory_Core.BLL;
using Inventory_Core.DAL;

namespace Inventory_GUI
{
    public partial class TransferAccessoriesToOtherOrderForm : BaseForm
    {
        public TransferAccessoriesToOtherOrderForm()
        {
            InitializeComponent();
        }

        public TransferAccessoriesToOtherOrderForm(string styleNo) : this()
        {
            fromStyleTextBox.Text = styleNo;
        }

        private void TransferAccessoriesToOtherOrderForm_Load(object sender, EventArgs e)
        {
            ClearFields();
        }

        private void ClearFields()
        {
            transferIdLabel.Text = string.Empty;
            transferDateTimePicker.Value = DateTime.Now;
            referenceTextBox.Text = string.Empty;

            fromStyleTextBox.Text = string.Empty;
            toStyleTextBox.Text = string.Empty;
            fromOrderLabel.Text = string.Empty;
            toOrderLabel.Text = string.Empty;

            transferDataGridView.DataSource = null;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            ClearFields();
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            if (fromStyleTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show(@"Enter from Style no.");
                return;
            }

            if (toStyleTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show(@"Enter to Style no.");
                return;
            }

            if (fromOrderLabel.Text.Length == 0 || toOrderLabel.Text.Length == 0)
            {
                MessageBox.Show(@"Invalid from or to style no.");
                return;        
            }

            var result = new InterOrderAccessoriesTransferManager().GetTransferableAccessories(
                fromStyleTextBox.Text.Trim(), toStyleTextBox.Text.Trim());

            transferDataGridView.AutoGenerateColumns = false;
            transferDataGridView.DataSource = result;
        }

        private void fromStyleTextBox_Validating(object sender, CancelEventArgs e)
        {
            string styleNo = fromStyleTextBox.Text.Trim();

            if (styleNo.Length == 0)
            {
                transferDataGridView.DataSource = null;
                fromOrderLabel.Text = string.Empty;
                return;
            }

            BuyerOrder buyerOrder = new BuyerOrderManager().GetBuyerOrderByStyleNo(styleNo);

            if (buyerOrder == null)
            {
                MessageBox.Show(@"Invalid style no.");
                transferDataGridView.DataSource = null;
                fromOrderLabel.Text = string.Empty;
                return;
            }

            fromOrderLabel.Text = buyerOrder.OrderId;
        }

        private void toStyleTextBox_Validating(object sender, CancelEventArgs e)
        {
            string styleNo = toStyleTextBox.Text.Trim();

            if (styleNo.Length == 0)
            {
                transferDataGridView.DataSource = null;
                toOrderLabel.Text = string.Empty;
                return;
            }

            BuyerOrder buyerOrder = new BuyerOrderManager().GetBuyerOrderByStyleNo(styleNo);

            if (buyerOrder == null)
            {
                MessageBox.Show(@"Invalid style no.");
                transferDataGridView.DataSource = null;
                toOrderLabel.Text = string.Empty;
                return;
            }

            if (buyerOrder.IsClosed)
            {
                MessageBox.Show(@"Order is closed.");
                transferDataGridView.DataSource = null;
                toOrderLabel.Text = string.Empty;
                return;
            }

            toOrderLabel.Text = buyerOrder.OrderId;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (fromOrderLabel.Text.Trim().Length == 0)
            {
                MessageBox.Show(@"Enter valid from style number");
                fromStyleTextBox.Focus();
                return;
            }

            if (toOrderLabel.Text.Trim().Length == 0)
            {
                MessageBox.Show(@"Enter valid to style number");
                toStyleTextBox.Focus();
                return;
            }

            if (transferDataGridView.Rows.Count == 0)
            {
                MessageBox.Show(@"No data found to save.");
                return;
            }

            var transferDetails = new EntityCollection<InterOrderAccessroiesTransferDetail>();

            decimal totalTransferQuantity = 0;

            for (int i = 0; i < transferDataGridView.Rows.Count; i++)
            {
                var transferDetail = new InterOrderAccessroiesTransferDetail();

                transferDetail.AccessoriesId = transferDataGridView.Rows[i].Cells["AccessoriesId"].Value.ToString();
                transferDetail.AttributeId = Convert.ToInt32(transferDataGridView.Rows[i].Cells["SpecId"].Value);
                transferDetail.Size = Convert.ToString(transferDataGridView.Rows[i].Cells["Size"].Value);
                transferDetail.MUnitId = Convert.ToInt32(transferDataGridView.Rows[i].Cells["MeasurementId"].Value);

                if (Convert.ToString(transferDataGridView.Rows[i].Cells["TransferQuantity"].Value) == string.Empty)
                {
                    transferDetail.Quantity = 0;
                }
                else
                {
                    transferDetail.Quantity = Convert.ToDecimal(transferDataGridView.Rows[i].Cells["TransferQuantity"].Value);
                }

                decimal currentStock = Convert.ToDecimal(transferDataGridView.Rows[i].Cells["CurrentBalance"].Value);

                if (currentStock < transferDetail.Quantity)
                {
                    MessageBox.Show(@"Transfer quantity cannot be greater than current balance. Row number: " + i);
                    return;
                }

                totalTransferQuantity += transferDetail.Quantity;
                transferDetails.Add(transferDetail);
            }

            if (totalTransferQuantity == 0)
            {
                MessageBox.Show(@"No transfer quantity found to save.");
                return;
            }

            var newTransfer = new InterOrderAccessroiesTransfer();

            newTransfer.TransferDate = transferDateTimePicker.Value;
            newTransfer.FromBuyerOrder = fromOrderLabel.Text;
            newTransfer.ToBuyerOrder = toOrderLabel.Text;

            
            newTransfer.EntryUserId = LoginInformation.User.UserId;
            newTransfer.EntryDate = DateTime.Now;
            newTransfer.InterOrderAccessroiesTransferDetails = transferDetails;

            try
            {
                new InterOrderAccessoriesTransferManager().SaveTransfer(newTransfer);

                transferIdLabel.Text = newTransfer.TransferId;

                MessageBox.Show(@"New transfer created successfully. Transfer Id: " + newTransfer.TransferId);

                ClearFields();
            }
            catch (Exception exception)
            {
                MessageBox.Show(@"Error: " + exception.Message);
                throw;
            }
        }

        
    }
}
