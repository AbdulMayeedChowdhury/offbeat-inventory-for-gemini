﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class PurchaseOrderManager
    {
        private OffbeatInventoryEntities _entities;
        private int newSerialNo;
        private SerialManager _serialManager;

        public PurchaseOrderManager()
        {
            _entities = new OffbeatInventoryEntities();  
        }

        public AccessoriesPurchaseOrder GetPurchaseOrder(string poId)
        {
            var purchaseOrder = _entities.AccessoriesPurchaseOrders.FirstOrDefault(o => o.POId == poId);

            return purchaseOrder;
        }

        public AccessoriesPurchaseOrder GetPurchaseOrderByStyleNo(string styleNo)
        {
            var buyerOrder = _entities.BuyerOrders.FirstOrDefault(o => o.StyleNo == styleNo);

            if (buyerOrder == null)
            {
                return null;
            }

            var purchaseOrder = _entities.AccessoriesPurchaseOrders.FirstOrDefault(po=> po.BuyerOrderId == buyerOrder.OrderId);

            return purchaseOrder;
        }

        public bool IsBuyerOrderExists(string buyerOrderId)
        {
            var purchaseOrder = _entities.AccessoriesPurchaseOrders.SingleOrDefault(po => po.BuyerOrderId == buyerOrderId);

            return purchaseOrder != null;
        }

        public void SavePurchaseOrder(AccessoriesPurchaseOrder newPurchaseOrder)
        {
            try
            {
                using (_entities)
                {
                    _serialManager = new SerialManager(_entities);
                    
                    newPurchaseOrder.POId = GetNewPOId();
                    newPurchaseOrder.IsActive = true;

                    foreach (var detail in newPurchaseOrder.AccessoriesPurchaseOrderDetails)
                    {
                        detail.PreviousTotalReceivedQuantity = 0;
                    }

                    _entities.AddToAccessoriesPurchaseOrders(newPurchaseOrder);

                    _serialManager.UpdateSerialNo(DateTime.Now, newSerialNo, SerialScope.PurchaseOrder);

                    _entities.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        private string GetNewPOId()
        {
            newSerialNo = _serialManager.GetLastSerialNo(DateTime.Now, SerialScope.PurchaseOrder) + 1;

            DateTime currentDate = DateTime.Now.Date;

            string yearPart = currentDate.Year.ToString().Substring(2, 2);
            string monthPart = "00" + currentDate.Month.ToString();
            monthPart = monthPart.Substring(monthPart.Length - 2, 2);

            string dayPart = "00" + currentDate.Day.ToString();
            dayPart = dayPart.Substring(dayPart.Length - 2, 2);

            string datePart = yearPart + monthPart + dayPart;

            string serialNo = "0000" + newSerialNo.ToString();

            string newOrderId = datePart + ((int)SerialScope.PurchaseOrder).ToString() + serialNo.Substring(serialNo.Length - 4, 4);

            return newOrderId;
        }

        public List<GetPurchaseOrderInformation_Result> GetPurchasablesByStyleNo(string styleNo)
        {
            List<GetPurchaseOrderInformation_Result> result;

            using (_entities)
            {
                result = _entities.GetPurchaseOrderInformation(styleNo).ToList();
            }

            return result;
        }
    }
}
