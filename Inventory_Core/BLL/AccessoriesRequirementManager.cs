﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class AccessoriesRequirementManager
    {
        private OffbeatInventoryEntities _entities;

        public AccessoriesRequirementManager()
        {
            _entities = new OffbeatInventoryEntities(); 
        }

        public void SaveAccessoriesRequired(EntityCollection<BuyerOrderAccessoriesRequired> accessoriesRequiredList)
        {
            try
            {
                using (_entities)
                {
                    foreach (var accessoriesRequired in accessoriesRequiredList)
                    {
                        var item = _entities.BuyerOrderAccessoriesRequireds.SingleOrDefault(r=> r.OrderId == accessoriesRequired.OrderId &&
                                                                                                r.AccessoriesId == accessoriesRequired.AccessoriesId &&
                                                                                                r.AttributeId == accessoriesRequired.AttributeId);

                        if (item == null)
                        {
                            _entities.AddToBuyerOrderAccessoriesRequireds(accessoriesRequired);
                        }
                        else
                        {
                            item.Quantity = accessoriesRequired.Quantity;
                        }
                    }

                    _entities.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message, exception); 
            }
        }
    }
}
