﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Inventory_Core.DAL;
using Inventory_Core.BLL;

namespace Inventory_GUI
{
    public partial class DashboardForm : BaseForm
    {
        private class OrderDetail
        {
            public string ItemCode { get; set; }
            public string ItemName { get; set; }
            public int SpecId { get; set; }
            public string SpecName { get; set; }
            public string Size { get; set; }
            public int MUnitId { get; set; }
            public string MUnitName { get; set; }
            public decimal Quantity { get; set; }
        }

        public DashboardForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                ClearFields();
            }
            catch (Exception exception)
            {
                MessageBox.Show(@"Error: " + exception.Message);
                Application.Exit();
            }
        }

        private void buyerOrderButton_Click(object sender, EventArgs e)
        {
            var buyerOrderForm = new BuyerOrderForm();
            buyerOrderForm.ShowDialog();
        }

        private void accessoriesRequirementButton_Click(object sender, EventArgs e)
        {
            if (IsValidStyleNumberExists())
            {
                var accessoriesRequirementForm = new AccessoriesRequirementForm(styleNoTextBox.Text.Trim());
                accessoriesRequirementForm.ShowDialog();

                LoadStyleRelatedInformation(styleNoTextBox.Text.Trim());
            }
        }

        private void purchaseOrderButton_Click(object sender, EventArgs e)
        {
            if (IsValidStyleNumberExists())
            {
                var purchaseOrderForm = new PurchaseOrderForm(styleNoTextBox.Text.Trim());
                purchaseOrderForm.ShowDialog();

                LoadStyleRelatedInformation(styleNoTextBox.Text.Trim());
            }
        }

        private void receivingButton_Click(object sender, EventArgs e)
        {
            if (IsValidStyleNumberExists())
            {
                var receivingForm = new ReceivingForm(styleNoTextBox.Text.Trim());
                receivingForm.ShowDialog();

                LoadStyleRelatedInformation(styleNoTextBox.Text.Trim());
            }
        }

        private void deliverButton_Click(object sender, EventArgs e)
        {
            if (IsValidStyleNumberExists())
            {
                var deliveryForm = new DeliveryForm(styleNoTextBox.Text.Trim());
                deliveryForm.ShowDialog();

                LoadStyleRelatedInformation(styleNoTextBox.Text.Trim());
            }
        }

        private void printButton_Click(object sender, EventArgs e)
        {
            if (IsValidStyleNumberExists())
            {
                var reportViewer = new PrintBuyerOrderSummary(styleNoTextBox.Text.Trim());
                reportViewer.ShowDialog();
            }
        }

        private void receivingFromOtherStyleButton_Click(object sender, EventArgs e)
        {
            if (IsValidStyleNumberExists())
            {
                var transferToOtherOrderForm = new TransferAccessoriesToOtherOrderForm(styleNoTextBox.Text.Trim());
                transferToOtherOrderForm.ShowDialog();

                LoadStyleRelatedInformation(styleNoTextBox.Text.Trim());
            }
        }

        private void closeOrderButton_Click(object sender, EventArgs e)
        {
            string styleNo = styleNoTextBox.Text.Trim();

            if (styleNo.Length == 0)
            {
                ClearFields();

                return;
            }

            try
            {
                var bayerOrderManager = new BuyerOrderManager();

                BuyerOrder buyerOrder = bayerOrderManager.GetBuyerOrderByStyleNo(styleNoTextBox.Text.Trim());

                if (!buyerOrder.IsClosed)
                {
                    bayerOrderManager.CloseBuyerOrder(buyerOrder);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(@"Failed to close order." + Environment.NewLine + @"Error: " + exception.Message);
                buttonGroupBox.Enabled = false;
            }

        }

        private void styleNoTextBox_Validating(object sender, CancelEventArgs e)
        {
            string styleNo = styleNoTextBox.Text.Trim();

            if (styleNo.Length == 0)
            {
                ClearFields();

                return;
            }

            try
            {
                LoadStyleRelatedInformation(styleNo);
            }
            catch (Exception exception)
            {
                MessageBox.Show(@"Could not load data." + Environment.NewLine + @"Error: " + exception.Message);
                buttonGroupBox.Enabled = false;
            }
        }

        private void LoadStyleRelatedInformation(string styleNo)
        {
            var bayerOrderManager = new BuyerOrderManager();

            BuyerOrder buyerOrder = bayerOrderManager.GetBuyerOrderByStyleNo(styleNo);

            if (buyerOrder == null)
            {
                MessageBox.Show(@"No such order found.");
                ClearFields();

                return;
            }

            buyerLabel.Text = buyerOrder.Buyer.BuyerName;
            orderIdLabel.Text = buyerOrder.OrderId;

            var orderDetails = new List<OrderDetail>();

            foreach (var detail in buyerOrder.BuyerOrderDetails)
            {
                var orderDetail = new OrderDetail();

                orderDetail.ItemCode = detail.ItemCode;
                orderDetail.ItemName = detail.Item.ItemName;
                orderDetail.SpecId = detail.AttributeId;
                orderDetail.SpecName = detail.Attribute.AttributeName;
                orderDetail.Size = detail.Size; 
                orderDetail.MUnitId = detail.MUnitId;
                orderDetail.MUnitName = detail.MeasurementUnit.MUnitName;
                orderDetail.Quantity = detail.Quantity;

                orderDetails.Add(orderDetail);
            }

            orderDetailDataGridView.AutoGenerateColumns = false;
            orderDetailDataGridView.DataSource = orderDetails;

            List<GetOrderSummary_Result> result = new ReportManager().GetOrderSummary(styleNo);

            if (result == null)
            {
                return;
            }

            summaryDataGridView.AutoGenerateColumns = false;
            summaryDataGridView.DataSource = result;

            buttonGroupBox.Enabled = true;
        }

        private void ClearFields()
        {
            styleNoTextBox.Text = string.Empty;
            buyerLabel.Text = string.Empty;
            orderIdLabel.Text = string.Empty;

            ClearOrderDetailsGrid();
            ClearSummaryGrid();

            styleNoTextBox.Focus();
        }

        private void ClearOrderDetailsGrid()
        {
            orderDetailDataGridView.DataSource = null;
        }

        private void ClearSummaryGrid()
        {
            summaryDataGridView.DataSource = null;
        }

        private bool IsValidStyleNumberExists()
        {
            if (styleNoTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show(@"Enter a valid style number.");
                styleNoTextBox.Focus();
                return false;
            }

            if (orderIdLabel.Text.Length == 0)
            {
                MessageBox.Show(@"Enter a valid style number.");
                styleNoTextBox.Focus();
                return false;
            }

            return true;
        }

    }
}
