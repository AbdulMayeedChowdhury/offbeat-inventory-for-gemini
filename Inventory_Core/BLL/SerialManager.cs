﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory_Core.DAL;
using System.Data.Objects;

namespace Inventory_Core.BLL
{
    public class SerialManager
    {
        private OffbeatInventoryEntities _entities;

        public SerialManager(OffbeatInventoryEntities entities)
        {
            _entities = entities;   
        }

        internal int GetLastSerialNo(DateTime serialDate, SerialScope serialScope)
        {
            int lastSerialNo = 0;

            LastIdSerial serialObject = _entities.LastIdSerials.FirstOrDefault(l => EntityFunctions.TruncateTime(l.SerialDate) == serialDate.Date);

            if (serialScope == SerialScope.BuyerOrder)
            {
                if (serialObject == null)
                {
                    lastSerialNo = 0;
                }
                else 
                {
                    lastSerialNo = serialObject.LastSerialForBuyerOrderId == null ? 0 : (int)serialObject.LastSerialForBuyerOrderId;
                }
            }

            if (serialScope == SerialScope.PurchaseOrder)
            {
                if (serialObject == null)
                {
                    lastSerialNo = 0;
                }
                else 
                {
                    lastSerialNo = serialObject.LastSerialForPurchaseOrderId == null ? 0 : (int)serialObject.LastSerialForPurchaseOrderId;
                }
            }

            if (serialScope == SerialScope.Receiving)
            {
                if (serialObject == null)
                {
                    lastSerialNo = 0;
                }
                else 
                {
                    lastSerialNo = serialObject.LastSerialForReceivingId == null ? 0 : (int)serialObject.LastSerialForReceivingId;
                }
            }

            if (serialScope == SerialScope.Delivery)
            {
                if (serialObject == null)
                {
                    lastSerialNo = 0;
                }
                else 
                {
                    lastSerialNo = serialObject.LastSerialForDeliveryId == null ? 0 : (int)serialObject.LastSerialForDeliveryId;
                }
            }

            if (serialScope == SerialScope.InterOrderTransfer)
            {
                if (serialObject == null)
                {
                    lastSerialNo = 0;
                }
                else
                {
                    lastSerialNo = serialObject.LastSerialForInterOrderTransferId == null ? 0 : (int)serialObject.LastSerialForInterOrderTransferId;
                }
            }

            return lastSerialNo;
        }
    
        internal void UpdateSerialNo(DateTime dateTime,int newSerialNo,SerialScope serialScope)
        {
 	        LastIdSerial serialObject = _entities.LastIdSerials.FirstOrDefault(l => l.SerialDate == dateTime.Date);
            
            if(serialObject == null)
            {
                if (serialScope == SerialScope.BuyerOrder)
	            {
                    serialObject = new LastIdSerial {SerialDate = dateTime.Date, LastSerialForBuyerOrderId = newSerialNo};
	            }
                else if(serialScope == SerialScope.PurchaseOrder)
                {
                    serialObject = new LastIdSerial {SerialDate = dateTime.Date, LastSerialForPurchaseOrderId = newSerialNo};
                }
                else if(serialScope == SerialScope.Receiving)
                {
                    serialObject = new LastIdSerial {SerialDate = dateTime.Date, LastSerialForReceivingId = newSerialNo};
                }
                else if(serialScope == SerialScope.Delivery)
                {
                    serialObject = new LastIdSerial {SerialDate = dateTime.Date, LastSerialForDeliveryId = newSerialNo};
                }
                else if (serialScope == SerialScope.InterOrderTransfer)
                {
                    serialObject = new LastIdSerial { SerialDate = dateTime.Date, LastSerialForInterOrderTransferId = newSerialNo };
                }

                _entities.AddToLastIdSerials(serialObject);
            }
            else
            {
                if (serialScope == SerialScope.BuyerOrder)
	            {
                    serialObject.LastSerialForBuyerOrderId = newSerialNo;
	            }
                else if(serialScope == SerialScope.PurchaseOrder)
                {
                    serialObject.LastSerialForPurchaseOrderId = newSerialNo;
                }
                else if(serialScope == SerialScope.Receiving)
                {
                    serialObject.LastSerialForReceivingId = newSerialNo;
                }
                else if(serialScope == SerialScope.Delivery)
                {
                    serialObject.LastSerialForDeliveryId = newSerialNo;
                }
                else if (serialScope == SerialScope.InterOrderTransfer)
                {
                    serialObject.LastSerialForInterOrderTransferId = newSerialNo;
                }
            }
        }
    }
}
