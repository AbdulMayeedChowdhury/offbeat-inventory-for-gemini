﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Inventory_Core.BLL;
using Inventory_Core.DAL;

namespace Inventory_GUI
{
    public partial class SpecificationSetupForm : BaseForm
    {
        public SpecificationSetupForm()
        {
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (specificationNameTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show("Enter specification name");
                return;
            }

            var attribute = new Inventory_Core.DAL.Attribute { AttributeName = specificationNameTextBox.Text.Trim(), IsActive = true };

            var attributeManager = new AttributeManager();

            try
            {
                attributeManager.SaveAttribute(attribute);

                MessageBox.Show("Specification saved successfully");
            }
            catch (Exception exception)
            {
                MessageBox.Show("Failed to save" + Environment.NewLine + "Error: " + exception.Message);
            }

            LoadSpecifications();
        }

        private void SpecificationSetupForm_Load(object sender, EventArgs e)
        {
            LoadSpecifications();
        }

        private void LoadSpecifications()
        {
            try
            {
                List<Inventory_Core.DAL.Attribute> attributes = new AttributeManager().GetAttributes(false);

                specificationDataGridView.AutoGenerateColumns = false;
                specificationDataGridView.DataSource = attributes;
            }
            catch (Exception exception)
            {
                MessageBox.Show("Failed to load specifications" + Environment.NewLine + "Error: " + exception.Message);
            }
        }
    }
}
