﻿namespace Inventory_GUI
{
    partial class MUnitSetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.measurementDataGridView = new System.Windows.Forms.DataGridView();
            this.measurementNameTextBox = new System.Windows.Forms.TextBox();
            this.MUnitId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MUnitName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.measurementDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(229, 32);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 21;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(150, 32);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 20;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Measurement Name";
            // 
            // measurementDataGridView
            // 
            this.measurementDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.measurementDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MUnitId,
            this.MUnitName});
            this.measurementDataGridView.Location = new System.Drawing.Point(15, 75);
            this.measurementDataGridView.Name = "measurementDataGridView";
            this.measurementDataGridView.Size = new System.Drawing.Size(289, 244);
            this.measurementDataGridView.TabIndex = 22;
            // 
            // measurementNameTextBox
            // 
            this.measurementNameTextBox.Location = new System.Drawing.Point(120, 6);
            this.measurementNameTextBox.Name = "measurementNameTextBox";
            this.measurementNameTextBox.Size = new System.Drawing.Size(184, 20);
            this.measurementNameTextBox.TabIndex = 19;
            // 
            // MUnitId
            // 
            this.MUnitId.DataPropertyName = "MUnitId";
            this.MUnitId.HeaderText = "Id";
            this.MUnitId.Name = "MUnitId";
            this.MUnitId.ReadOnly = true;
            // 
            // MUnitName
            // 
            this.MUnitName.DataPropertyName = "MUnitName";
            this.MUnitName.HeaderText = "Name";
            this.MUnitName.Name = "MUnitName";
            this.MUnitName.ReadOnly = true;
            // 
            // MUnitSetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 334);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.measurementDataGridView);
            this.Controls.Add(this.measurementNameTextBox);
            this.Name = "MUnitSetupForm";
            this.Text = "Measurement Unit Setup";
            this.Load += new System.EventHandler(this.MUnitSetupForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.measurementDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView measurementDataGridView;
        private System.Windows.Forms.TextBox measurementNameTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn MUnitId;
        private System.Windows.Forms.DataGridViewTextBoxColumn MUnitName;
    }
}