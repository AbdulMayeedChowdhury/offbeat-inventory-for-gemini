﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Inventory_Core.BLL;
using Inventory_Core.DAL;

namespace Inventory_GUI
{
    public partial class PrintBuyerOrderSummary : Form
    {
        public string[] _parameters;

        public PrintBuyerOrderSummary(params string[] parameters)
        {
            InitializeComponent();

            _parameters = parameters;
        }

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            var reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            var reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();

            List<GetOrderSummary_Result> summary = new ReportManager().GetOrderSummary(_parameters[0]);
            reportDataSource1.Name = "BuyerOrderSummaryDataSet";
            reportDataSource1.Value = summary;

            List<GetOrderedItems_Result> itemDetails = new ReportManager().GetOrderedItems(_parameters[0]);
            reportDataSource2.Name = "BuyerOrderItemDetailsDataSet";
            reportDataSource2.Value = itemDetails;

            reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            reportViewer1.LocalReport.DataSources.Add(reportDataSource2);

            reportViewer1.LocalReport.ReportPath = Application.StartupPath + @"\Report\OrderSummaryReport.rdlc";
            this.reportViewer1.RefreshReport();

        }
    }
}
