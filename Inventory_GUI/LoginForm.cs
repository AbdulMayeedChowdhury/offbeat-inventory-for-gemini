﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Inventory_Core.BLL;
using Inventory_Core.DAL;

namespace Inventory_GUI
{
    public partial class LoginForm : Form
    {
        public bool IsValidLogin { get; set; }
        
        public LoginForm()
        {
            InitializeComponent();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            string userId = userIdTextBox.Text.Trim();
            string password = passwordTextBox.Text;

            if (userId.Length == 0)
            {
                MessageBox.Show(@"Enter user i:Dd", @"Validation");
                return;
            }

            if (password.Length == 0)
            {
                MessageBox.Show(@"Enter password", @"Validation");
                return;
            }

            try
            {
                var loginUserManager = new LoginUserManager();

                LoginUser user = loginUserManager.GetValidLoginUser(userId, password);

                if (user == null)
                {
                    MessageBox.Show(@"Invalid user id or password", @"Validation");
                    return;
                }

                LoginInformation.User = user;

                this.Hide();
            }
            catch (Exception exception)
            {
                MessageBox.Show(@"Login Failed. " + Environment.NewLine + @"Error: " + exception.Message +
                                Environment.NewLine + exception.InnerException.Message);
                //throw new Exception("Login failed. Error: " + exception.InnerException.Message, exception);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
