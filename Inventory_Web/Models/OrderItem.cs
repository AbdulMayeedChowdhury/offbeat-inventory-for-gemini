﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory_Web.Models
{
    public class OrderItem
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int SpecId { get; set; }
        public string SpecName { get; set; }
        public string Size { get; set; }
        public int MUnitId { get; set; }
        public string MUnitName { get; set; }
        public decimal Quantity { get; set; }
    }
}