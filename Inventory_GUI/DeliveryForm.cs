﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Inventory_Core.BLL;
using Inventory_Core.DAL;
using System.Data.Objects.DataClasses;

namespace Inventory_GUI
{
    public partial class DeliveryForm : BaseForm
    {
        private string _styleNo = string.Empty;

        private class OrderDetail
        {
            public string ItemCode { get; set; }
            public string ItemName { get; set; }
            public int SpecId { get; set; }
            public string SpecName { get; set; }
            public string Size { get; set; }
            public int MUnitId { get; set; }
            public string MUnitName { get; set; }
            public decimal Quantity { get; set; }
        }

        #region Constructor

        public DeliveryForm()
        {
            InitializeComponent();
        }

        public DeliveryForm(string styleNo) : this()
        {
            _styleNo = styleNo;
        }

        #endregion

        #region Event Handler

        private void DeliveryForm_Load(object sender, EventArgs e)
        {
            ClearFields();

            if (_styleNo.Length > 0)
            {
                styleNoTextBox.Text = _styleNo;
                styleNoTextBox.Enabled = false;
                LoadStyleRelatedInformation(_styleNo);
            }
        }

        private void styleNoTextBox_Validating(object sender, CancelEventArgs e)
        {
            string styleNo = styleNoTextBox.Text.Trim();

            if (styleNo.Length == 0)
            {
                buyerLabel.Text = string.Empty;
                orderIdLabel.Text = string.Empty;
                orderDetailDataGridView.DataSource = null;
                deliveryDataGridView.DataSource = null;

                return;
            }

            LoadStyleRelatedInformation(styleNo);
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (styleNoTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show("Enter style number");
                styleNoTextBox.Focus();
                return;
            }

            if (deliveryDataGridView.Rows.Count == 0)
            {
                MessageBox.Show("No data found to save.");
                styleNoTextBox.Focus();
                return;
            }

            var deliveryDetails = new EntityCollection<AccessoriesDeliveredToProductionDetail>();

            decimal totalDeliverQuantity = 0;

            for (int i = 0; i <= deliveryDataGridView.Rows.Count - 1; i++)
            {
                var deliveryDetail = new AccessoriesDeliveredToProductionDetail();

                deliveryDetail.AccessoriesId = deliveryDataGridView.Rows[i].Cells["AccessoriesId"].Value.ToString();
                deliveryDetail.AttributeId = Convert.ToInt32(deliveryDataGridView.Rows[i].Cells["SpecId"].Value);
                deliveryDetail.Size = Convert.ToString(deliveryDataGridView.Rows[i].Cells["Size"].Value);
                deliveryDetail.MUnitId = Convert.ToInt32(deliveryDataGridView.Rows[i].Cells["MeasurementId"].Value);

                if (Convert.ToString(deliveryDataGridView.Rows[i].Cells["DeliverQuantity"].Value) == string.Empty)
                {
                    deliveryDetail.Quantity = 0;
                }
                else
                {
                    deliveryDetail.Quantity = Convert.ToDecimal(deliveryDataGridView.Rows[i].Cells["DeliverQuantity"].Value);
                }

                totalDeliverQuantity += deliveryDetail.Quantity;

                if (deliveryDetail.Quantity > 0)
                {
                    deliveryDetails.Add(deliveryDetail);
                }
            }

            if (totalDeliverQuantity == 0)
            {
                MessageBox.Show("No receive quantity found to save.");
                return;
            }

            var newDelivery = new AccessoriesDeliveredToProduction();

            newDelivery.DeliveryDate = deliveryDateTimePicker.Value;
            newDelivery.BuyerOrderId = orderIdLabel.Text;

            if (referenceTextBox.Text.Trim().Length > 0)
            {
                newDelivery.Remarks = referenceTextBox.Text.Trim();
            }

            newDelivery.EntryUserId = LoginInformation.User.UserId;
            newDelivery.EntryDate = DateTime.Now;
            newDelivery.AccessoriesDeliveredToProductionDetails = deliveryDetails;

            try
            {
                new DeliveryManager().SaveDelivery(newDelivery);

                deliveryIdLabel.Text = newDelivery.DeliveryId;

                MessageBox.Show("New delivery created successfully. Delivery Id: " + newDelivery.DeliveryId);

                ClearFields();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message);
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            if (_styleNo.Length == 0)
            {
                ClearFields();
            }
        }

        #endregion

        #region Private Methods

        private void LoadAccessoriesForDelivery(string styleNo)
        {
            var deliverableInformation = new DeliveryManager().GetDeliverablesByStyleNo(styleNo);

            deliveryDataGridView.AutoGenerateColumns = false;
            deliveryDataGridView.DataSource = deliverableInformation;
        }

        private void ClearFields()
        {
            deliveryIdLabel.Text = string.Empty;
            deliveryDateTimePicker.Value = DateTime.Now;
            referenceTextBox.Text = string.Empty;
            styleNoTextBox.Text = string.Empty;
            buyerLabel.Text = string.Empty;
            orderIdLabel.Text = string.Empty;

            orderDetailDataGridView.DataSource = null;
            deliveryDataGridView.DataSource = null;
        }

        private void LoadStyleRelatedInformation(string styleNo)
        {
            BuyerOrder buyerOrder = new BuyerOrderManager().GetBuyerOrderByStyleNo(styleNo);

            if (buyerOrder == null)
            {
                buyerLabel.Text = string.Empty;
                orderIdLabel.Text = string.Empty;
                orderDetailDataGridView.DataSource = null;
                deliveryDataGridView.DataSource = null;

                return;
            }

            if (buyerOrder.IsClosed)
            {
                MessageBox.Show(@"This order is closed");
                buyerLabel.Text = string.Empty;
                orderIdLabel.Text = string.Empty;
                orderDetailDataGridView.DataSource = null;
                deliveryDataGridView.DataSource = null;

                return;
            }

            buyerLabel.Text = buyerOrder.Buyer.BuyerName;
            orderIdLabel.Text = buyerOrder.OrderId;

            var orderDetails = new List<OrderDetail>();

            foreach (var detail in buyerOrder.BuyerOrderDetails)
            {
                var orderDetail = new OrderDetail();

                orderDetail.ItemCode = detail.ItemCode;
                orderDetail.ItemName = detail.Item.ItemName;
                orderDetail.SpecId = detail.AttributeId;
                orderDetail.SpecName = detail.Attribute.AttributeName;
                orderDetail.Size = detail.Size;
                orderDetail.MUnitId = detail.MUnitId;
                orderDetail.MUnitName = detail.MeasurementUnit.MUnitName;
                orderDetail.Quantity = detail.Quantity;

                orderDetails.Add(orderDetail);
            }

            orderDetailDataGridView.AutoGenerateColumns = false;
            orderDetailDataGridView.DataSource = orderDetails;

            if (buyerOrder.BuyerOrderAccessoriesRequireds != null)
            {
                LoadAccessoriesForDelivery(styleNo);
            }
        }

        #endregion

    }
}
