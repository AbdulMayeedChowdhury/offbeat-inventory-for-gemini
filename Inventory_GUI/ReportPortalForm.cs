﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Inventory_GUI
{
    public partial class ReportPortalForm : BaseForm
    {
        public ReportPortalForm()
        {
            InitializeComponent();
        }

        private void accessoriesStockButton_Click(object sender, EventArgs e)
        {
            var stockReportForm = new AccessoriesStockReportForm();
            stockReportForm.ShowDialog();
        }

        private void accessoriesReceivingReportButton_Click(object sender, EventArgs e)
        {

        }
    }
}
