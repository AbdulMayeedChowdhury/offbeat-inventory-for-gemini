﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Inventory_GUI
{
    public partial class StartupForm : Form
    {
        public StartupForm()
        {
            InitializeComponent();
        }

        private void StartupForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoginProcess();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Login Failed." + Environment.NewLine + "Error: " + exception.Message, "System Message");
                Application.Exit();
            }
        }

        private void reportButton_Click(object sender, EventArgs e)
        {
            var reportPortalForm = new ReportPortalForm();
            reportPortalForm.ShowDialog();
        }

        private void dashboardButton_Click(object sender, EventArgs e)
        {
            var dashboardForm = new DashboardForm();
            dashboardForm.ShowDialog();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            LoginProcess();
        }

        private static void LoginProcess()
        {
            LoginForm loginForm = new LoginForm();
            loginForm.ShowDialog();

            if (loginForm.IsValidLogin)
            {
                loginForm.Close();
            }
        }

        private void createLoginUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var loginUserSetup = new LoginUserSetup();
            loginUserSetup.ShowDialog();
        }

        private void itemSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //var itemSetupForm = new ItemSetup
        }

        private void accessoriesSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var accessoriesSetupForm = new AccessorySetupForm();
            accessoriesSetupForm.ShowDialog();
        }

        private void unitSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var munitSetupForm = new MUnitSetupForm();
            munitSetupForm.ShowDialog();
        }

        private void specificationSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var specificationSetupForm = new SpecificationSetupForm();
            specificationSetupForm.ShowDialog();
        }

        private void orderDashboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dashboardForm = new DashboardForm();
            dashboardForm.ShowDialog();
        }

        private void createBuyerOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var buyerOrderForm = new BuyerOrderForm();
            buyerOrderForm.ShowDialog();
        }

        private void accessoriesRequirementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var accessoriesRequirementForm = new AccessoriesRequirementForm();
            accessoriesRequirementForm.ShowDialog();
        }

        private void purchaseOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var purchaseOrderForm = new PurchaseOrderForm();
            purchaseOrderForm.ShowDialog();
        }

        private void receivingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var receivingForm = new ReceivingForm();
            receivingForm.ShowDialog();
        }

        private void deliveryToProductionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var deliveryForm = new DeliveryForm();
            deliveryForm.ShowDialog();
        }

        private void transferToOtherStyleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var transferToOtherStyleForm = new TransferAccessoriesToOtherOrderForm();
            transferToOtherStyleForm.Show();
        }




    }
}
