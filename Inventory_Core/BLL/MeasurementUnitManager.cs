﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class MeasurementUnitManager
    {
        private OffbeatInventoryEntities _entities;

        public MeasurementUnitManager()
        {
            _entities = new OffbeatInventoryEntities();    
        }

        public List<MeasurementUnit> GetMeasurementUnits()
        {
            var measurements = _entities.MeasurementUnits.ToList();
            
            return measurements;
        }

        public MeasurementUnit GetMeasurement(int measurementId)
        {
            var measurement = _entities.MeasurementUnits.FirstOrDefault(b => b.MUnitId == measurementId);

            return measurement;
        }

        public void SaveMeasurement(MeasurementUnit measurement)
        {
            try
            {
                using (_entities)
                {
                    _entities.AddToMeasurementUnits(measurement);
                    _entities.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }
    }
}
