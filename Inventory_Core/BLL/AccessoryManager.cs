﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class AccessoryManager
    {
        private OffbeatInventoryEntities _entities;

        public AccessoryManager()
        {
            _entities = new OffbeatInventoryEntities();    
        }

        public List<Accessory> GetAccessories(bool isActiveOnly)
        {
            List<Accessory> accessories;

            if (isActiveOnly)
            {
                accessories = _entities.Accessories.Where(a => a.IsActive == true).ToList();
            }
            else
            {
                accessories = _entities.Accessories.ToList();
            }

            return accessories;
        }

        public List<Accessory> GetAccessories(int categoryId, bool isActiveOnly)
        {
            List<Accessory> accessories;

            if (isActiveOnly)
            {
                accessories = _entities.Accessories.Where(a => a.CategoryId == categoryId && a.IsActive).ToList();
            }
            else
            {
                accessories = _entities.Accessories.Where(a => a.CategoryId == categoryId).ToList();
            }

            return accessories;
        }

        public Accessory GetAccessory(string accessoryId)
        {
            var accessory = _entities.Accessories.FirstOrDefault(a => a.AccessoriesId == accessoryId);

            return accessory;
        }

        public void SaveAccessory(Accessory accessory)
        {
            try
            {
                using (_entities)
                {

                    if (string.IsNullOrEmpty(accessory.AccessoriesId))
                    {
                        string newAccessoryId;

                        var lastAccessoryId = _entities.Accessories.Max(a => a.AccessoriesId);

                        if (string.IsNullOrEmpty(lastAccessoryId))
                        {
                            newAccessoryId = "0000000001";
                        }
                        else
                        {
                            int newId = Convert.ToInt32(lastAccessoryId) + 1;
                            newAccessoryId = "0000000000" + newId;
                            newAccessoryId = newAccessoryId.Substring(newAccessoryId.Length - 10, 10);
                        }

                        accessory.AccessoriesId = newAccessoryId;

                        _entities.AddToAccessories(accessory); 
                    }
                    else
                    {
                        Accessory existingAccessory = _entities.Accessories.FirstOrDefault(a=>a.AccessoriesId == accessory.AccessoriesId);

                        if (existingAccessory == null)
                        {
                            throw new Exception(@"Accessory not found to update.");
                        }

                        existingAccessory.AccessoriesName = accessory.AccessoriesName;
                        existingAccessory.CategoryId = accessory.CategoryId;
                    }

                    _entities.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }
    }
}
