﻿namespace Inventory_GUI
{
    partial class DeliveryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.deliveryIdLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.deliveryDataGridView = new System.Windows.Forms.DataGridView();
            this.referenceTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.deliveryDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.clearButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.orderIdLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.orderDetailDataGridView = new System.Windows.Forms.DataGridView();
            this.styleNoTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buyerLabel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemAttributeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemAttributeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemMeasurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccessoriesId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccessoriesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Detail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SpecId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SpecName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MeasurementId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RequiredQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlreadyDelivered = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliverQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deliveryDataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(540, 46);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(200, 21);
            this.comboBox1.TabIndex = 48;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(436, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 40;
            this.label4.Text = "Receiving Date";
            // 
            // deliveryIdLabel
            // 
            this.deliveryIdLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.deliveryIdLabel.Location = new System.Drawing.Point(91, 15);
            this.deliveryIdLabel.Name = "deliveryIdLabel";
            this.deliveryIdLabel.Size = new System.Drawing.Size(151, 21);
            this.deliveryIdLabel.TabIndex = 39;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 38;
            this.label3.Text = "Delivery Id";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.deliveryDataGridView);
            this.groupBox2.Location = new System.Drawing.Point(10, 196);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(971, 315);
            this.groupBox2.TabIndex = 37;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Delivery Information";
            // 
            // deliveryDataGridView
            // 
            this.deliveryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.deliveryDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AccessoriesId,
            this.AccessoriesName,
            this.Detail,
            this.SpecId,
            this.SpecName,
            this.Size,
            this.MeasurementId,
            this.Measurement,
            this.RequiredQuantity,
            this.AlreadyDelivered,
            this.CurrentBalance,
            this.DeliverQuantity});
            this.deliveryDataGridView.Location = new System.Drawing.Point(9, 21);
            this.deliveryDataGridView.Name = "deliveryDataGridView";
            this.deliveryDataGridView.Size = new System.Drawing.Size(956, 284);
            this.deliveryDataGridView.TabIndex = 31;
            // 
            // referenceTextBox
            // 
            this.referenceTextBox.Location = new System.Drawing.Point(93, 44);
            this.referenceTextBox.Name = "referenceTextBox";
            this.referenceTextBox.Size = new System.Drawing.Size(264, 20);
            this.referenceTextBox.TabIndex = 46;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 45;
            this.label10.Text = "Reference";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(439, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 47;
            this.label11.Text = "From Area";
            // 
            // deliveryDateTimePicker
            // 
            this.deliveryDateTimePicker.Location = new System.Drawing.Point(540, 15);
            this.deliveryDateTimePicker.Name = "deliveryDateTimePicker";
            this.deliveryDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.deliveryDateTimePicker.TabIndex = 41;
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(890, 63);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(91, 33);
            this.clearButton.TabIndex = 44;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(890, 107);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(91, 33);
            this.exitButton.TabIndex = 43;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(890, 24);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(91, 33);
            this.saveButton.TabIndex = 42;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.orderIdLabel);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.orderDetailDataGridView);
            this.groupBox1.Controls.Add(this.styleNoTextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.buyerLabel);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Location = new System.Drawing.Point(10, 69);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(869, 123);
            this.groupBox1.TabIndex = 49;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Buyer Order Information";
            // 
            // orderIdLabel
            // 
            this.orderIdLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.orderIdLabel.Location = new System.Drawing.Point(63, 81);
            this.orderIdLabel.Name = "orderIdLabel";
            this.orderIdLabel.Size = new System.Drawing.Size(224, 21);
            this.orderIdLabel.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 81);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Order Id";
            // 
            // orderDetailDataGridView
            // 
            this.orderDetailDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderDetailDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.ItemAttributeId,
            this.ItemAttributeName,
            this.ItemSize,
            this.ItemMeasurement,
            this.ItemQuantity});
            this.orderDetailDataGridView.Location = new System.Drawing.Point(337, 18);
            this.orderDetailDataGridView.Name = "orderDetailDataGridView";
            this.orderDetailDataGridView.Size = new System.Drawing.Size(519, 96);
            this.orderDetailDataGridView.TabIndex = 22;
            // 
            // styleNoTextBox
            // 
            this.styleNoTextBox.Location = new System.Drawing.Point(63, 29);
            this.styleNoTextBox.Name = "styleNoTextBox";
            this.styleNoTextBox.Size = new System.Drawing.Size(100, 20);
            this.styleNoTextBox.TabIndex = 17;
            this.styleNoTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.styleNoTextBox_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Style No.";
            // 
            // buyerLabel
            // 
            this.buyerLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.buyerLabel.Location = new System.Drawing.Point(63, 55);
            this.buyerLabel.Name = "buyerLabel";
            this.buyerLabel.Size = new System.Drawing.Size(264, 20);
            this.buyerLabel.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 55);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Buyer";
            // 
            // ItemCode
            // 
            this.ItemCode.DataPropertyName = "ItemCode";
            this.ItemCode.HeaderText = "Code";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.ReadOnly = true;
            this.ItemCode.Visible = false;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // ItemAttributeId
            // 
            this.ItemAttributeId.DataPropertyName = "SpecId";
            this.ItemAttributeId.HeaderText = "Spec. Id";
            this.ItemAttributeId.Name = "ItemAttributeId";
            this.ItemAttributeId.ReadOnly = true;
            this.ItemAttributeId.Visible = false;
            // 
            // ItemAttributeName
            // 
            this.ItemAttributeName.DataPropertyName = "SpecName";
            this.ItemAttributeName.HeaderText = "Spec/Color";
            this.ItemAttributeName.Name = "ItemAttributeName";
            this.ItemAttributeName.ReadOnly = true;
            // 
            // ItemSize
            // 
            this.ItemSize.DataPropertyName = "Size";
            this.ItemSize.HeaderText = "Size";
            this.ItemSize.Name = "ItemSize";
            // 
            // ItemMeasurement
            // 
            this.ItemMeasurement.DataPropertyName = "MUnitName";
            this.ItemMeasurement.HeaderText = "Unit";
            this.ItemMeasurement.Name = "ItemMeasurement";
            this.ItemMeasurement.ReadOnly = true;
            // 
            // ItemQuantity
            // 
            this.ItemQuantity.DataPropertyName = "Quantity";
            this.ItemQuantity.HeaderText = "Quantity";
            this.ItemQuantity.Name = "ItemQuantity";
            this.ItemQuantity.ReadOnly = true;
            // 
            // AccessoriesId
            // 
            this.AccessoriesId.DataPropertyName = "AccessoriesId";
            this.AccessoriesId.HeaderText = "Accessories Id";
            this.AccessoriesId.Name = "AccessoriesId";
            this.AccessoriesId.ReadOnly = true;
            this.AccessoriesId.Visible = false;
            // 
            // AccessoriesName
            // 
            this.AccessoriesName.DataPropertyName = "AccessoriesName";
            this.AccessoriesName.HeaderText = "Accessories Name";
            this.AccessoriesName.Name = "AccessoriesName";
            this.AccessoriesName.ReadOnly = true;
            // 
            // Detail
            // 
            this.Detail.DataPropertyName = "Detail";
            this.Detail.HeaderText = "Detail";
            this.Detail.Name = "Detail";
            this.Detail.ReadOnly = true;
            // 
            // SpecId
            // 
            this.SpecId.DataPropertyName = "AttributeId";
            this.SpecId.HeaderText = "Spec Id";
            this.SpecId.Name = "SpecId";
            this.SpecId.ReadOnly = true;
            this.SpecId.Visible = false;
            // 
            // SpecName
            // 
            this.SpecName.DataPropertyName = "AttributeName";
            this.SpecName.HeaderText = "Spec/Color";
            this.SpecName.Name = "SpecName";
            this.SpecName.ReadOnly = true;
            // 
            // Size
            // 
            this.Size.DataPropertyName = "Size";
            this.Size.HeaderText = "Size";
            this.Size.Name = "Size";
            // 
            // MeasurementId
            // 
            this.MeasurementId.DataPropertyName = "MUnit";
            this.MeasurementId.HeaderText = "MUnit Id";
            this.MeasurementId.Name = "MeasurementId";
            this.MeasurementId.ReadOnly = true;
            this.MeasurementId.Visible = false;
            // 
            // Measurement
            // 
            this.Measurement.DataPropertyName = "MUnitName";
            this.Measurement.HeaderText = "Unit";
            this.Measurement.Name = "Measurement";
            this.Measurement.ReadOnly = true;
            // 
            // RequiredQuantity
            // 
            this.RequiredQuantity.DataPropertyName = "RequiredQuantity";
            this.RequiredQuantity.HeaderText = "Required";
            this.RequiredQuantity.Name = "RequiredQuantity";
            this.RequiredQuantity.ReadOnly = true;
            // 
            // AlreadyDelivered
            // 
            this.AlreadyDelivered.DataPropertyName = "DeliveredQuantity";
            this.AlreadyDelivered.HeaderText = "Already Delivered";
            this.AlreadyDelivered.Name = "AlreadyDelivered";
            // 
            // CurrentBalance
            // 
            this.CurrentBalance.DataPropertyName = "CurrentStock";
            this.CurrentBalance.HeaderText = "Current Balance";
            this.CurrentBalance.Name = "CurrentBalance";
            this.CurrentBalance.ReadOnly = true;
            // 
            // DeliverQuantity
            // 
            this.DeliverQuantity.HeaderText = "Deliver Quantity";
            this.DeliverQuantity.Name = "DeliverQuantity";
            // 
            // DeliveryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(995, 514);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.deliveryIdLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.referenceTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.deliveryDateTimePicker);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.saveButton);
            this.Name = "DeliveryForm";
            this.Text = "Delivery To Production";
            this.Load += new System.EventHandler(this.DeliveryForm_Load);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deliveryDataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label deliveryIdLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox referenceTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker deliveryDateTimePicker;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView orderDetailDataGridView;
        private System.Windows.Forms.TextBox styleNoTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label buyerLabel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label orderIdLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView deliveryDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccessoriesId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccessoriesName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Detail;
        private System.Windows.Forms.DataGridViewTextBoxColumn SpecId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SpecName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn MeasurementId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn RequiredQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlreadyDelivered;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliverQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemAttributeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemAttributeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemMeasurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemQuantity;
    }
}