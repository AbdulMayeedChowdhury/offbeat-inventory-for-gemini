﻿using Inventory_Core.DAL;
using Inventory_Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory_Web.ViewModels
{
    public class DashboardViewModel
    {
        public BuyerOrder BuyerOrder { get; set; }

        public List<OrderItem> ItemDetails { get; set; }

        public List<GetOrderSummary_Result> AccessoriesDetails { get; set; }
    }
}