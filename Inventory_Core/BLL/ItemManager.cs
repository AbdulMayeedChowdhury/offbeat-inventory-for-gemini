﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class ItemManager
    {
        private OffbeatInventoryEntities _entities;

        public ItemManager()
        {
            _entities = new OffbeatInventoryEntities();    
        }

        public List<Item> GetItems(bool isActiveOnly)
        {
            var items = new List<Item>();

            if (isActiveOnly)
            {
                items = _entities.Items.Where(i => i.IsActive == true).ToList();
            }
            else
            {
                items = _entities.Items.ToList();
            }

            return items;
        }

        public Item GetItem(string itemCode)
        {
            var item = _entities.Items.FirstOrDefault(i => i.ItemCode == itemCode);

            return item;
        }
    }
}
