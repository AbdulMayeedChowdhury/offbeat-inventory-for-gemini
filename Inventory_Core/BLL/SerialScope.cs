﻿namespace Inventory_Core.BLL
{
    public enum SerialScope
    { 
        BuyerOrder = 1,
        PurchaseOrder = 2,
        Receiving = 3,
        Delivery = 4,
        InterOrderTransfer=5
    }
}
