﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Inventory_Core.BLL;
using Inventory_Core.DAL;

namespace Inventory_GUI
{
    public partial class BuyerSetupForm : BaseForm
    {
        public BuyerSetupForm()
        {
            InitializeComponent();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (buyerNameTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show("Enter buyer name");
                return;
            }

            var buyer = new Buyer { BuyerName = buyerNameTextBox.Text.Trim() };
            
            var buyerManager = new BuyerManager();

            try
            {
                buyerManager.SaveBuyer(buyer);

                MessageBox.Show("Buyer saved successfully");
            }
            catch (Exception exception)
            {
                MessageBox.Show("Failed to save" + Environment.NewLine + "Error: " + exception.Message);
            }

            LoadBuyers();
        }

        private void BuyerSetupForm_Load(object sender, EventArgs e)
        {
            LoadBuyers();
        }

        private void LoadBuyers()
        {
            try
            {
                List<Buyer> buyers = new BuyerManager().GetBuyers();

                buyerDataGridView.AutoGenerateColumns = false;
                buyerDataGridView.DataSource = buyers;
            }
            catch (Exception exception)
            {
                MessageBox.Show("Failed to load buyers" + Environment.NewLine + "Error: " + exception.Message);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
