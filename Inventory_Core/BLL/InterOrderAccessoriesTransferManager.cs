﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class InterOrderAccessoriesTransferManager
    {
        private OffbeatInventoryEntities _entities;
        private int _newSerialNo;
        private SerialManager _serialManager;

        public InterOrderAccessoriesTransferManager()
        {
            _entities = new OffbeatInventoryEntities(); 
        }

        public List<GetTransferableAccessories_Result> GetTransferableAccessories(string fromStyleNo, string toStyleNo)
        {
            List<GetTransferableAccessories_Result> result;

            using (_entities)
            {
                result = _entities.GetTransferableAccessories(fromStyleNo, toStyleNo).ToList();
            }

            return result;
        }

        public void SaveTransfer(InterOrderAccessroiesTransfer newTransfer)
        {
            try
            {
                using (_entities)
                {
                    _serialManager = new SerialManager(_entities);

                    newTransfer.TransferId = GetNewTransferId();
                    newTransfer.IsActive = true;
                    newTransfer.AreaCode = 1;

                    _entities.AddToInterOrderAccessroiesTransfers(newTransfer);

                    _serialManager.UpdateSerialNo(DateTime.Now, _newSerialNo, SerialScope.Receiving);

                    _entities.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        private string GetNewTransferId()
        {
            _newSerialNo = _serialManager.GetLastSerialNo(DateTime.Now, SerialScope.InterOrderTransfer) + 1;

            DateTime currentDate = DateTime.Now.Date;

            string yearPart = currentDate.Year.ToString().Substring(2, 2);
            string monthPart = "00" + currentDate.Month;
            monthPart = monthPart.Substring(monthPart.Length - 2, 2);

            string dayPart = "00" + currentDate.Day;
            dayPart = dayPart.Substring(dayPart.Length - 2, 2);

            string datePart = yearPart + monthPart + dayPart;

            string serialNo = "0000" + _newSerialNo;

            string newId = datePart + ((int)SerialScope.InterOrderTransfer) + serialNo.Substring(serialNo.Length - 4, 4);

            return newId;
        }
    }
}
