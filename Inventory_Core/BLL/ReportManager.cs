﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class ReportManager
    {
        private OffbeatInventoryEntities _entities;

        public ReportManager()
        {
            _entities = new OffbeatInventoryEntities();  
        }

        public List<GetOrderSummary_Result> GetOrderSummary(string styleNo)
        {
            List<GetOrderSummary_Result> result;

            using (_entities)
            {
                result = _entities.SP_GetOrderSummary(styleNo).ToList();
            }

            return result;
        }

        public List<GetOrderedItems_Result> GetOrderedItems(string styleNo)
        {
            List<GetOrderedItems_Result> result;

            using (_entities)
            {
                result = _entities.GetOrderedItems(styleNo).ToList();
            }

            return result;
        }
    }
}
