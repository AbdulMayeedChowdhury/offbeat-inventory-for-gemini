﻿namespace Inventory_GUI
{
    partial class ReportPortalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buyerOrderReportButton = new System.Windows.Forms.Button();
            this.accessoriesPOReportButton = new System.Windows.Forms.Button();
            this.accessoriesReceivingReportButton = new System.Windows.Forms.Button();
            this.accessoriesDeliveryButton = new System.Windows.Forms.Button();
            this.accessoriesStockButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buyerOrderReportButton
            // 
            this.buyerOrderReportButton.Location = new System.Drawing.Point(18, 39);
            this.buyerOrderReportButton.Name = "buyerOrderReportButton";
            this.buyerOrderReportButton.Size = new System.Drawing.Size(102, 40);
            this.buyerOrderReportButton.TabIndex = 0;
            this.buyerOrderReportButton.Text = "Buyer Order Report";
            this.buyerOrderReportButton.UseVisualStyleBackColor = true;
            // 
            // accessoriesPOReportButton
            // 
            this.accessoriesPOReportButton.Location = new System.Drawing.Point(140, 39);
            this.accessoriesPOReportButton.Name = "accessoriesPOReportButton";
            this.accessoriesPOReportButton.Size = new System.Drawing.Size(102, 40);
            this.accessoriesPOReportButton.TabIndex = 1;
            this.accessoriesPOReportButton.Text = "Accessories PO Report";
            this.accessoriesPOReportButton.UseVisualStyleBackColor = true;
            // 
            // accessoriesReceivingReportButton
            // 
            this.accessoriesReceivingReportButton.Location = new System.Drawing.Point(262, 39);
            this.accessoriesReceivingReportButton.Name = "accessoriesReceivingReportButton";
            this.accessoriesReceivingReportButton.Size = new System.Drawing.Size(102, 40);
            this.accessoriesReceivingReportButton.TabIndex = 2;
            this.accessoriesReceivingReportButton.Text = "Accessories Receiving Report";
            this.accessoriesReceivingReportButton.UseVisualStyleBackColor = true;
            this.accessoriesReceivingReportButton.Click += new System.EventHandler(this.accessoriesReceivingReportButton_Click);
            // 
            // accessoriesDeliveryButton
            // 
            this.accessoriesDeliveryButton.Location = new System.Drawing.Point(386, 39);
            this.accessoriesDeliveryButton.Name = "accessoriesDeliveryButton";
            this.accessoriesDeliveryButton.Size = new System.Drawing.Size(102, 40);
            this.accessoriesDeliveryButton.TabIndex = 3;
            this.accessoriesDeliveryButton.Text = "Accessories Delivery Report";
            this.accessoriesDeliveryButton.UseVisualStyleBackColor = true;
            // 
            // accessoriesStockButton
            // 
            this.accessoriesStockButton.Location = new System.Drawing.Point(140, 108);
            this.accessoriesStockButton.Name = "accessoriesStockButton";
            this.accessoriesStockButton.Size = new System.Drawing.Size(224, 40);
            this.accessoriesStockButton.TabIndex = 5;
            this.accessoriesStockButton.Text = "Accessories Stock";
            this.accessoriesStockButton.UseVisualStyleBackColor = true;
            this.accessoriesStockButton.Click += new System.EventHandler(this.accessoriesStockButton_Click);
            // 
            // ReportPortalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 178);
            this.Controls.Add(this.accessoriesStockButton);
            this.Controls.Add(this.accessoriesDeliveryButton);
            this.Controls.Add(this.accessoriesReceivingReportButton);
            this.Controls.Add(this.accessoriesPOReportButton);
            this.Controls.Add(this.buyerOrderReportButton);
            this.Name = "ReportPortalForm";
            this.Text = "Report Portal";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buyerOrderReportButton;
        private System.Windows.Forms.Button accessoriesPOReportButton;
        private System.Windows.Forms.Button accessoriesReceivingReportButton;
        private System.Windows.Forms.Button accessoriesDeliveryButton;
        private System.Windows.Forms.Button accessoriesStockButton;
    }
}