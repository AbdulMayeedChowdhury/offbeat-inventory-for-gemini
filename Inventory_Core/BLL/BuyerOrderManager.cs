﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class BuyerOrderManager
    {
        private OffbeatInventoryEntities _entities;
        private int newSerialNo;
        private SerialManager _serialManager;

        public BuyerOrderManager()
        {
            _entities = new OffbeatInventoryEntities();  
        }

        public BuyerOrder GetBuyerOrder(string orderId)
        {
            var order = _entities.BuyerOrders.FirstOrDefault(o => o.OrderId == orderId);

            return order;
        }
        
        public BuyerOrder GetBuyerOrderByStyleNo(string styleNo)
        {
            var order = _entities.BuyerOrders.FirstOrDefault(o => o.StyleNo == styleNo);

            return order;
        }

        public bool IsStyleNoExists(string styleNo)
        {
            var buyerOrder = _entities.BuyerOrders.SingleOrDefault(b => b.StyleNo == styleNo);

            return buyerOrder != null;
        }

        public void SaveBuyerOrder(BuyerOrder newBuyerOrder)
        {
            try
            {
                using (_entities)
                {
                    _serialManager = new SerialManager(_entities);

                    newBuyerOrder.OrderId = GetNewOrderId(); 

                    _entities.AddToBuyerOrders(newBuyerOrder);

                    _serialManager.UpdateSerialNo(DateTime.Now, newSerialNo, SerialScope.BuyerOrder);

                    _entities.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        public void CloseBuyerOrder(BuyerOrder buyerOrder)
        {
            try
            {
                using (_entities)
                {
                    var order = _entities.BuyerOrders.SingleOrDefault(bo=> bo.OrderId == buyerOrder.OrderId);

                    if (order == null)
                    {
                        throw new Exception("Order not found.");
                    }

                    order.IsClosed = true;

                    _entities.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        private string GetNewOrderId()
        {
            newSerialNo = _serialManager.GetLastSerialNo(DateTime.Now, SerialScope.BuyerOrder) + 1;

            DateTime currentDate = DateTime.Now.Date;

            string yearPart = currentDate.Year.ToString().Substring(2, 2);
            string monthPart = "00" + currentDate.Month.ToString();
            monthPart = monthPart.Substring(monthPart.Length - 2, 2);

            string dayPart = "00" + currentDate.Day.ToString();
            dayPart = dayPart.Substring(dayPart.Length - 2, 2);

            string datePart = yearPart + monthPart + dayPart;

            string serialNo = "0000" + newSerialNo.ToString();

            string newOrderId = datePart + ((int)SerialScope.BuyerOrder).ToString() + serialNo.Substring(serialNo.Length - 4, 4);

            return newOrderId;
        }

        //public List<GetOrderSummary_Result> GetOrderSummary(string styleNo)
        //{
        //    List<GetOrderSummary_Result> result;

        //    using (_entities)
        //    {
        //        result = _entities.GetOrderSummary(styleNo).ToList();
        //    }

        //    return result;
        //}
    }
}
