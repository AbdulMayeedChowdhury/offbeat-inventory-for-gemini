﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Inventory_Core.DAL;
using Inventory_Core.BLL;
using System.Data.Objects.DataClasses;

namespace Inventory_GUI
{
    public partial class BuyerOrderForm : BaseForm
    {
        public BuyerOrderForm()
        {
            InitializeComponent();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BuyerOrderForm_Load(object sender, EventArgs e)
        {
            LoadBuyerList();
            LoadItemList();
            LoadSpecificationList();
            LoadMeasurementList();
            LoadDataGridViewMeasurementComboList();
        }

        private void LoadBuyerList()
        {
            List<Buyer> buyers = new BuyerManager().GetBuyers();

            buyers.Insert(0, new Buyer { BuyerId = -1, BuyerName = "--Select Buyer--" });
            buyers.Insert(buyers.Count, new Buyer{BuyerId = 0, BuyerName = "--New Buyer--"});

            buyerComboBox.ValueMember = "BuyerId";
            buyerComboBox.DisplayMember = "BuyerName";

            buyerComboBox.DataSource = buyers;
        }

        private void LoadItemList()
        {
            List<Item> items = new ItemManager().GetItems(true);

            items.Insert(0, new Item { ItemCode = "select", ItemName = "--Select Item--" });
            //items.Insert(items.Count, new Item { ItemCode = "new", ItemName = "--New Item--" });

            itemComboBox.ValueMember = "ItemCode";
            itemComboBox.DisplayMember = "ItemName";

            itemComboBox.DataSource = items;
        }

        public void LoadSpecificationList()
        {
            List<Inventory_Core.DAL.Attribute> attributes = new AttributeManager().GetAttributes(true);

            attributes.Insert(0, new Inventory_Core.DAL.Attribute { AttributeId = -1, AttributeName = "--Select Specification--" });
            attributes.Insert(attributes.Count, new Inventory_Core.DAL.Attribute { AttributeId = 0, AttributeName = "--New Attribute--" });

            specificationComboBox.ValueMember = "AttributeId";
            specificationComboBox.DisplayMember = "AttributeName";

            specificationComboBox.DataSource = attributes;
        }

        private void LoadMeasurementList()
        {
            List<MeasurementUnit> measurements = new MeasurementUnitManager().GetMeasurementUnits();

            measurements.Insert(0, new MeasurementUnit { MUnitId = -1, MUnitName = "--Select Measurement--" });
            measurements.Insert(measurements.Count, new MeasurementUnit { MUnitId = 0, MUnitName = "--New Measurement--" });

            measurementComboBox.ValueMember = "MUnitId";
            measurementComboBox.DisplayMember = "MUnitName";

            measurementComboBox.DataSource = measurements;
        }

        private void LoadDataGridViewMeasurementComboList()
        {
            DataGridViewComboBoxColumn comboColumn = (DataGridViewComboBoxColumn)orderDataGridView.Columns["Measurement"];

            List<MeasurementUnit> measurements = new MeasurementUnitManager().GetMeasurementUnits();

            comboColumn.DisplayMember = "MUnitName";
            comboColumn.ValueMember = "MUnitId";
            comboColumn.DataSource = measurements;
        }

        private void buyerComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (buyerComboBox.SelectedValue.ToString() == "-1")
            {
                return;
            }

            if (buyerComboBox.SelectedValue.ToString() == "0")
            {
                var buyerSetupForm = new BuyerSetupForm();
                buyerSetupForm.ShowDialog();

                LoadBuyerList();
            }
        }

        private void quantityTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar) || (e.KeyChar == (char)Keys.Back) || (e.KeyChar == (char)Keys.Delete))) 
                e.Handled = true;
            else
                e.Handled = false;
        }

        private void specificationComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (specificationComboBox.SelectedValue.ToString() == "-1")
            {
                return;
            }

            if (specificationComboBox.SelectedValue.ToString() == "0")
            {
                var specificationSetupForm = new SpecificationSetupForm();
                specificationSetupForm.ShowDialog();

                LoadSpecificationList();
            }
        }

        private void measurementComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (measurementComboBox.SelectedValue.ToString() == "-1")
            {
                return;
            }

            if (measurementComboBox.SelectedValue.ToString() == "0")
            {
                var munitSetupForm = new MUnitSetupForm();
                munitSetupForm.ShowDialog();

                LoadMeasurementList();
            }
        }

        private void addToGridButton_Click(object sender, EventArgs e)
        {
            if (buyerComboBox.SelectedValue.ToString() == "-1")
            {
                MessageBox.Show("Select buyer.");
                return;
            }

            string styleNo = styleNoTextBox.Text.Trim();

            if (styleNo.Length == 0)
            {
                MessageBox.Show("Enter style no.");
                return;
            }

            if (new BuyerOrderManager().IsStyleNoExists(styleNo))
            {
                MessageBox.Show("Duplicate style no. Try again.");
                return;
            }

            if (itemComboBox.SelectedValue.ToString() == "-1")
            {
                MessageBox.Show("Select item.");
                return;
            }

            if (specificationComboBox.SelectedValue.ToString() == "-1")
            {
                MessageBox.Show("Select specification.");
                return;
            }

            if (measurementComboBox.SelectedValue.ToString() == "-1")
            {
                MessageBox.Show("Select Measurement.");
                return;
            }

            orderDataGridView.Rows.Add(itemComboBox.SelectedValue.ToString(), itemComboBox.Text, 
                (int)specificationComboBox.SelectedValue, specificationComboBox.Text, 
                sizeTextBox.Text.Trim() == string.Empty ? "-" : sizeTextBox.Text.Trim(),
                measurementComboBox.SelectedValue, Convert.ToDecimal(quantityTextBox.Text));
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (buyerComboBox.SelectedValue.ToString() == "-1")
            {
                MessageBox.Show("Select buyer");
                buyerComboBox.Focus();
                return;
            }

            if (styleNoTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show("Enter style number");
                styleNoTextBox.Focus();
                return;
            }

            if (orderDataGridView.Rows.Count == 0)
            {
                MessageBox.Show("No order details found to save");
                return;
            }
            
            var orderDetails = new EntityCollection<BuyerOrderDetail>();

            for (int i = 0; i < orderDataGridView.Rows.Count - 1; i++)
            {
                var orderDetail = new BuyerOrderDetail();

                orderDetail.ItemCode = orderDataGridView.Rows[i].Cells["ItemCode"].Value.ToString();
                orderDetail.AttributeId = Convert.ToInt32(orderDataGridView.Rows[i].Cells["SpecificationId"].Value);
                orderDetail.Size = Convert.ToString(orderDataGridView.Rows[i].Cells["Size"].Value);
                orderDetail.MUnitId = (int)((DataGridViewComboBoxCell)orderDataGridView.Rows[i].Cells["Measurement"]).Value;
                orderDetail.Quantity = Convert.ToDecimal(orderDataGridView.Rows[i].Cells["Quantity"].Value);

                orderDetails.Add(orderDetail);
            }
            
            BuyerOrder newBuyerOrder = new BuyerOrder();

            newBuyerOrder.OrderDate = orderDateTimePicker.Value;
            newBuyerOrder.StyleNo = styleNoTextBox.Text.Trim();
            newBuyerOrder.BuyerId = (int)buyerComboBox.SelectedValue;
            newBuyerOrder.EntryUserId = LoginInformation.User.UserId;
            newBuyerOrder.EntryDate = DateTime.Now;
            newBuyerOrder.BuyerOrderDetails = orderDetails;

            try
            {
                new BuyerOrderManager().SaveBuyerOrder(newBuyerOrder);

                orderNoLabel.Text = newBuyerOrder.OrderId;

                MessageBox.Show("New order created successfully with style number " + styleNoTextBox.Text.Trim());

                ClearFields();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message);
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            ClearFields();
        }

        private void ClearFields()
        {
            buyerComboBox.SelectedIndex = 0;
            orderNoLabel.Text = string.Empty;
            orderDateTimePicker.Value = DateTime.Now;
            styleNoTextBox.Text = string.Empty;
            itemComboBox.SelectedIndex = 0;
            specificationComboBox.SelectedIndex = 0;
            sizeTextBox.Text = string.Empty;
            measurementComboBox.SelectedIndex = 0;
            quantityTextBox.Text = string.Empty;

            orderDataGridView.DataSource = null;
        }

    }
}
