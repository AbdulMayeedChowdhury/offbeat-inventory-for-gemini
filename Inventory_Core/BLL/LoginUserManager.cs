﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class LoginUserManager
    {
        private OffbeatInventoryEntities _entities;

        public LoginUserManager()
        {
            _entities = new OffbeatInventoryEntities();            
        }

        public LoginUser GetValidLoginUser(string userId, string password)
        {
            var user = _entities.LoginUsers.FirstOrDefault(u => u.UserId == userId && u.Password == password);

            return user;
        }

        public LoginUser GetLoginUser(string userId)
        {
            var user = _entities.LoginUsers.FirstOrDefault(u => u.UserId == userId);

            return user;
        }

        public List<LoginUser> GetLoginUsers()
        {
            if (_entities == null)
            {
                _entities = new OffbeatInventoryEntities();
            }

            return _entities.LoginUsers.ToList();
        }

        public bool IsUserExists(string userId)
        {
            LoginUser user = GetLoginUser(userId);

            return user != null;
        }

        public void SaveLoginUser(LoginUser user)
        {
            try
            {
                using (_entities)
                {
                    LoginUser existingUser = _entities.LoginUsers.FirstOrDefault(u => u.UserId == user.UserId);

                    if (existingUser == null)
                    {
                        _entities.AddToLoginUsers(user);
                    }
                    else
                    {
                        existingUser.FullName = user.FullName;
                        existingUser.Password = user.Password;
                    }

                    _entities.SaveChanges();                    
                    
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }
    }
}
