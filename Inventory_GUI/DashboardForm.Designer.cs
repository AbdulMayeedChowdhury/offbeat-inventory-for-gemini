﻿namespace Inventory_GUI
{
    partial class DashboardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buyerOrderButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.styleNoTextBox = new System.Windows.Forms.TextBox();
            this.summaryDataGridView = new System.Windows.Forms.DataGridView();
            this.AccessoriesId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccessoriesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Detail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SpecificationId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SpecificationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MUnitId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MUnitName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RequiredQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderedQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceiveQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceivingDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceiveFromOtherOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransferToOtherOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExcessQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveredQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeliveryDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Balance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buyerLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.orderIdLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.orderDetailDataGridView = new System.Windows.Forms.DataGridView();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemAttributeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemAttributeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemMeasurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonGroupBox = new System.Windows.Forms.GroupBox();
            this.receivingFromOtherStyleButton = new System.Windows.Forms.Button();
            this.closeOrderButton = new System.Windows.Forms.Button();
            this.printButton = new System.Windows.Forms.Button();
            this.deliverButton = new System.Windows.Forms.Button();
            this.receivingButton = new System.Windows.Forms.Button();
            this.purchaseOrderButton = new System.Windows.Forms.Button();
            this.accessoriesRequirementButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.summaryDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailDataGridView)).BeginInit();
            this.buttonGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // buyerOrderButton
            // 
            this.buyerOrderButton.Location = new System.Drawing.Point(210, 18);
            this.buyerOrderButton.Name = "buyerOrderButton";
            this.buyerOrderButton.Size = new System.Drawing.Size(102, 33);
            this.buyerOrderButton.TabIndex = 0;
            this.buyerOrderButton.Text = "New Buyer Order";
            this.buyerOrderButton.UseVisualStyleBackColor = true;
            this.buyerOrderButton.Click += new System.EventHandler(this.buyerOrderButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Style No.";
            // 
            // styleNoTextBox
            // 
            this.styleNoTextBox.Location = new System.Drawing.Point(72, 24);
            this.styleNoTextBox.Name = "styleNoTextBox";
            this.styleNoTextBox.Size = new System.Drawing.Size(107, 20);
            this.styleNoTextBox.TabIndex = 6;
            this.styleNoTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.styleNoTextBox_Validating);
            // 
            // summaryDataGridView
            // 
            this.summaryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.summaryDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AccessoriesId,
            this.AccessoriesName,
            this.Detail,
            this.SpecificationId,
            this.SpecificationName,
            this.Size,
            this.MUnitId,
            this.MUnitName,
            this.RequiredQuantity,
            this.OrderedQuantity,
            this.ReceiveQuantity,
            this.ReceivingDate,
            this.ReceiveFromOtherOrder,
            this.TransferToOtherOrder,
            this.ExcessQuantity,
            this.DeliveredQuantity,
            this.DeliveryDate,
            this.Balance});
            this.summaryDataGridView.Location = new System.Drawing.Point(6, 167);
            this.summaryDataGridView.Name = "summaryDataGridView";
            this.summaryDataGridView.Size = new System.Drawing.Size(1233, 522);
            this.summaryDataGridView.TabIndex = 7;
            // 
            // AccessoriesId
            // 
            this.AccessoriesId.DataPropertyName = "AccessoriesId";
            this.AccessoriesId.HeaderText = "Accessories Id";
            this.AccessoriesId.Name = "AccessoriesId";
            this.AccessoriesId.ReadOnly = true;
            this.AccessoriesId.Visible = false;
            // 
            // AccessoriesName
            // 
            this.AccessoriesName.DataPropertyName = "AccessoriesName";
            this.AccessoriesName.HeaderText = "Accessories Name";
            this.AccessoriesName.Name = "AccessoriesName";
            this.AccessoriesName.ReadOnly = true;
            // 
            // Detail
            // 
            this.Detail.DataPropertyName = "Detail";
            this.Detail.HeaderText = "Detail";
            this.Detail.Name = "Detail";
            this.Detail.ReadOnly = true;
            // 
            // SpecificationId
            // 
            this.SpecificationId.DataPropertyName = "AttributeID";
            this.SpecificationId.HeaderText = "Spec. Id";
            this.SpecificationId.Name = "SpecificationId";
            this.SpecificationId.ReadOnly = true;
            this.SpecificationId.Visible = false;
            // 
            // SpecificationName
            // 
            this.SpecificationName.DataPropertyName = "AttributeName";
            this.SpecificationName.HeaderText = "Spec./Color";
            this.SpecificationName.Name = "SpecificationName";
            this.SpecificationName.ReadOnly = true;
            // 
            // Size
            // 
            this.Size.DataPropertyName = "Size";
            this.Size.HeaderText = "Size";
            this.Size.Name = "Size";
            // 
            // MUnitId
            // 
            this.MUnitId.HeaderText = "MUnitId";
            this.MUnitId.Name = "MUnitId";
            this.MUnitId.ReadOnly = true;
            this.MUnitId.Visible = false;
            // 
            // MUnitName
            // 
            this.MUnitName.DataPropertyName = "MUnitName";
            this.MUnitName.HeaderText = "Unit";
            this.MUnitName.Name = "MUnitName";
            this.MUnitName.ReadOnly = true;
            // 
            // RequiredQuantity
            // 
            this.RequiredQuantity.DataPropertyName = "RequiredQuantity";
            this.RequiredQuantity.HeaderText = "Req. Qty";
            this.RequiredQuantity.Name = "RequiredQuantity";
            this.RequiredQuantity.ReadOnly = true;
            // 
            // OrderedQuantity
            // 
            this.OrderedQuantity.DataPropertyName = "POQuantity";
            this.OrderedQuantity.HeaderText = "Ordered";
            this.OrderedQuantity.Name = "OrderedQuantity";
            this.OrderedQuantity.ReadOnly = true;
            // 
            // ReceiveQuantity
            // 
            this.ReceiveQuantity.DataPropertyName = "ReceivedQuantity";
            this.ReceiveQuantity.HeaderText = "Received";
            this.ReceiveQuantity.Name = "ReceiveQuantity";
            this.ReceiveQuantity.ReadOnly = true;
            // 
            // ReceivingDate
            // 
            this.ReceivingDate.DataPropertyName = "LastReceivedDate";
            this.ReceivingDate.HeaderText = "Recv. Date";
            this.ReceivingDate.Name = "ReceivingDate";
            this.ReceivingDate.ReadOnly = true;
            // 
            // ReceiveFromOtherOrder
            // 
            this.ReceiveFromOtherOrder.DataPropertyName = "ReceiveQuantityFromOtherOrder";
            this.ReceiveFromOtherOrder.HeaderText = "Receive (Other Style)";
            this.ReceiveFromOtherOrder.Name = "ReceiveFromOtherOrder";
            // 
            // TransferToOtherOrder
            // 
            this.TransferToOtherOrder.DataPropertyName = "TransferQuantityToOtherOrder";
            this.TransferToOtherOrder.HeaderText = "Transfer (Other Style)";
            this.TransferToOtherOrder.Name = "TransferToOtherOrder";
            // 
            // ExcessQuantity
            // 
            this.ExcessQuantity.DataPropertyName = "ExcessQuanity";
            this.ExcessQuantity.HeaderText = "Excess/(Short)";
            this.ExcessQuantity.Name = "ExcessQuantity";
            this.ExcessQuantity.ReadOnly = true;
            // 
            // DeliveredQuantity
            // 
            this.DeliveredQuantity.DataPropertyName = "DeliveredQuantity";
            this.DeliveredQuantity.HeaderText = "Delivered";
            this.DeliveredQuantity.Name = "DeliveredQuantity";
            this.DeliveredQuantity.ReadOnly = true;
            // 
            // DeliveryDate
            // 
            this.DeliveryDate.DataPropertyName = "LastDeliveryDate";
            this.DeliveryDate.HeaderText = "Del. Date";
            this.DeliveryDate.Name = "DeliveryDate";
            this.DeliveryDate.ReadOnly = true;
            // 
            // Balance
            // 
            this.Balance.DataPropertyName = "CurrentStock";
            this.Balance.HeaderText = "Balance";
            this.Balance.Name = "Balance";
            // 
            // buyerLabel
            // 
            this.buyerLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.buyerLabel.Location = new System.Drawing.Point(73, 55);
            this.buyerLabel.Name = "buyerLabel";
            this.buyerLabel.Size = new System.Drawing.Size(240, 21);
            this.buyerLabel.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Buyer";
            // 
            // orderIdLabel
            // 
            this.orderIdLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.orderIdLabel.Location = new System.Drawing.Point(72, 85);
            this.orderIdLabel.Name = "orderIdLabel";
            this.orderIdLabel.Size = new System.Drawing.Size(224, 21);
            this.orderIdLabel.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Order Id";
            // 
            // orderDetailDataGridView
            // 
            this.orderDetailDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderDetailDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.ItemAttributeId,
            this.ItemAttributeName,
            this.ItemSize,
            this.ItemMeasurement,
            this.ItemQuantity});
            this.orderDetailDataGridView.Location = new System.Drawing.Point(346, 12);
            this.orderDetailDataGridView.Name = "orderDetailDataGridView";
            this.orderDetailDataGridView.Size = new System.Drawing.Size(545, 133);
            this.orderDetailDataGridView.TabIndex = 20;
            // 
            // ItemCode
            // 
            this.ItemCode.DataPropertyName = "ItemCode";
            this.ItemCode.HeaderText = "Code";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.ReadOnly = true;
            this.ItemCode.Visible = false;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // ItemAttributeId
            // 
            this.ItemAttributeId.DataPropertyName = "SpecId";
            this.ItemAttributeId.HeaderText = "Spec. Id";
            this.ItemAttributeId.Name = "ItemAttributeId";
            this.ItemAttributeId.ReadOnly = true;
            this.ItemAttributeId.Visible = false;
            // 
            // ItemAttributeName
            // 
            this.ItemAttributeName.DataPropertyName = "SpecName";
            this.ItemAttributeName.HeaderText = "Spec/Color";
            this.ItemAttributeName.Name = "ItemAttributeName";
            this.ItemAttributeName.ReadOnly = true;
            // 
            // ItemSize
            // 
            this.ItemSize.DataPropertyName = "Size";
            this.ItemSize.HeaderText = "Size";
            this.ItemSize.Name = "ItemSize";
            // 
            // ItemMeasurement
            // 
            this.ItemMeasurement.DataPropertyName = "MUnitName";
            this.ItemMeasurement.HeaderText = "Unit";
            this.ItemMeasurement.Name = "ItemMeasurement";
            this.ItemMeasurement.ReadOnly = true;
            // 
            // ItemQuantity
            // 
            this.ItemQuantity.DataPropertyName = "Quantity";
            this.ItemQuantity.HeaderText = "Quantity";
            this.ItemQuantity.Name = "ItemQuantity";
            this.ItemQuantity.ReadOnly = true;
            // 
            // buttonGroupBox
            // 
            this.buttonGroupBox.Controls.Add(this.receivingFromOtherStyleButton);
            this.buttonGroupBox.Controls.Add(this.closeOrderButton);
            this.buttonGroupBox.Controls.Add(this.printButton);
            this.buttonGroupBox.Controls.Add(this.deliverButton);
            this.buttonGroupBox.Controls.Add(this.receivingButton);
            this.buttonGroupBox.Controls.Add(this.purchaseOrderButton);
            this.buttonGroupBox.Controls.Add(this.accessoriesRequirementButton);
            this.buttonGroupBox.Location = new System.Drawing.Point(910, 5);
            this.buttonGroupBox.Name = "buttonGroupBox";
            this.buttonGroupBox.Size = new System.Drawing.Size(303, 146);
            this.buttonGroupBox.TabIndex = 22;
            this.buttonGroupBox.TabStop = false;
            // 
            // receivingFromOtherStyleButton
            // 
            this.receivingFromOtherStyleButton.Location = new System.Drawing.Point(107, 60);
            this.receivingFromOtherStyleButton.Name = "receivingFromOtherStyleButton";
            this.receivingFromOtherStyleButton.Size = new System.Drawing.Size(182, 35);
            this.receivingFromOtherStyleButton.TabIndex = 28;
            this.receivingFromOtherStyleButton.Text = "Receiving From Other Style";
            this.receivingFromOtherStyleButton.UseVisualStyleBackColor = true;
            this.receivingFromOtherStyleButton.Click += new System.EventHandler(this.receivingFromOtherStyleButton_Click);
            // 
            // closeOrderButton
            // 
            this.closeOrderButton.Location = new System.Drawing.Point(201, 101);
            this.closeOrderButton.Name = "closeOrderButton";
            this.closeOrderButton.Size = new System.Drawing.Size(88, 35);
            this.closeOrderButton.TabIndex = 27;
            this.closeOrderButton.Text = "Close Order";
            this.closeOrderButton.UseVisualStyleBackColor = true;
            this.closeOrderButton.Click += new System.EventHandler(this.closeOrderButton_Click);
            // 
            // printButton
            // 
            this.printButton.Location = new System.Drawing.Point(201, 15);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(88, 35);
            this.printButton.TabIndex = 26;
            this.printButton.Text = "Print";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // deliverButton
            // 
            this.deliverButton.Location = new System.Drawing.Point(13, 105);
            this.deliverButton.Name = "deliverButton";
            this.deliverButton.Size = new System.Drawing.Size(88, 35);
            this.deliverButton.TabIndex = 25;
            this.deliverButton.Text = "Deliver";
            this.deliverButton.UseVisualStyleBackColor = true;
            this.deliverButton.Click += new System.EventHandler(this.deliverButton_Click);
            // 
            // receivingButton
            // 
            this.receivingButton.Location = new System.Drawing.Point(13, 60);
            this.receivingButton.Name = "receivingButton";
            this.receivingButton.Size = new System.Drawing.Size(88, 35);
            this.receivingButton.TabIndex = 24;
            this.receivingButton.Text = "Receiving";
            this.receivingButton.UseVisualStyleBackColor = true;
            this.receivingButton.Click += new System.EventHandler(this.receivingButton_Click);
            // 
            // purchaseOrderButton
            // 
            this.purchaseOrderButton.Location = new System.Drawing.Point(107, 15);
            this.purchaseOrderButton.Name = "purchaseOrderButton";
            this.purchaseOrderButton.Size = new System.Drawing.Size(88, 35);
            this.purchaseOrderButton.TabIndex = 23;
            this.purchaseOrderButton.Text = "Purchase Order";
            this.purchaseOrderButton.UseVisualStyleBackColor = true;
            this.purchaseOrderButton.Click += new System.EventHandler(this.purchaseOrderButton_Click);
            // 
            // accessoriesRequirementButton
            // 
            this.accessoriesRequirementButton.Location = new System.Drawing.Point(13, 15);
            this.accessoriesRequirementButton.Name = "accessoriesRequirementButton";
            this.accessoriesRequirementButton.Size = new System.Drawing.Size(88, 35);
            this.accessoriesRequirementButton.TabIndex = 22;
            this.accessoriesRequirementButton.Text = "Accessories Requirement";
            this.accessoriesRequirementButton.UseVisualStyleBackColor = true;
            this.accessoriesRequirementButton.Click += new System.EventHandler(this.accessoriesRequirementButton_Click);
            // 
            // DashboardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1246, 694);
            this.Controls.Add(this.orderDetailDataGridView);
            this.Controls.Add(this.orderIdLabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.buyerLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.summaryDataGridView);
            this.Controls.Add(this.styleNoTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buyerOrderButton);
            this.Controls.Add(this.buttonGroupBox);
            this.Name = "DashboardForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Order Dashboard";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.summaryDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailDataGridView)).EndInit();
            this.buttonGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buyerOrderButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox styleNoTextBox;
        private System.Windows.Forms.DataGridView summaryDataGridView;
        private System.Windows.Forms.Label buyerLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label orderIdLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView orderDetailDataGridView;
        private System.Windows.Forms.GroupBox buttonGroupBox;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Button deliverButton;
        private System.Windows.Forms.Button receivingButton;
        private System.Windows.Forms.Button purchaseOrderButton;
        private System.Windows.Forms.Button accessoriesRequirementButton;
        private System.Windows.Forms.Button closeOrderButton;
        private System.Windows.Forms.Button receivingFromOtherStyleButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccessoriesId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccessoriesName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Detail;
        private System.Windows.Forms.DataGridViewTextBoxColumn SpecificationId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SpecificationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn MUnitId;
        private System.Windows.Forms.DataGridViewTextBoxColumn MUnitName;
        private System.Windows.Forms.DataGridViewTextBoxColumn RequiredQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderedQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceiveQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceivingDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceiveFromOtherOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransferToOtherOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExcessQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveredQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeliveryDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Balance;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemAttributeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemAttributeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemMeasurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemQuantity;
    }
}

