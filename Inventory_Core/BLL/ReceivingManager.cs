﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class ReceivingManager
    {
        private OffbeatInventoryEntities _entities;
        private int _newSerialNo;
        private SerialManager _serialManager;

        public ReceivingManager()
        {
            _entities = new OffbeatInventoryEntities();  
        }

        public AccessoriesReceiving GetReceiving(string receivingId)
        {
            var receiving = _entities.AccessoriesReceivings.FirstOrDefault(r => r.ReceivingId == receivingId);

            return receiving;
        }

        public List<AccessoriesReceiving> GetReceivingByStyleNo(string styleNo)
        {
            var buyerOrder = _entities.BuyerOrders.FirstOrDefault(o => o.StyleNo == styleNo);

            if (buyerOrder == null)
            {
                return null;
            }

            var purchaseOrder = _entities.AccessoriesPurchaseOrders.FirstOrDefault(po=> po.BuyerOrderId == buyerOrder.OrderId);

            if (purchaseOrder == null)
            {
                return null;
            }

            var receivings = _entities.AccessoriesReceivings.Where(r=> r.POId == purchaseOrder.POId).ToList();

            return receivings;
        }

        public void SaveReceiving(AccessoriesReceiving newReceiving)
        {
            try
            {
                using (_entities)
                {
                    _serialManager = new SerialManager(_entities);

                    var stockManager = new AccessoriesStockManager(_entities);

                    newReceiving.ReceivingId = GetNewReceivingId();
                    newReceiving.IsActive = true;
                    newReceiving.AreaCode = 1;

                    _entities.AddToAccessoriesReceivings(newReceiving);

                    foreach (var receiveDetail in newReceiving.AccessoriesReceivingDetails)
                    {
                        var poDetail = newReceiving.AccessoriesPurchaseOrder.AccessoriesPurchaseOrderDetails.Single(
                                                                d => d.AccessoriesId == receiveDetail.AccessoriesId && 
                                                                d.AttributeId == receiveDetail.AttributeId);

                        poDetail.PreviousTotalReceivedQuantity += receiveDetail.Quantity;

                        stockManager.IncreaseStock(receiveDetail.AccessoriesId, receiveDetail.AttributeId, receiveDetail.Size, 
                                                    receiveDetail.MUnitId, newReceiving.AreaCode, receiveDetail.Quantity);
                    }

                    _serialManager.UpdateSerialNo(DateTime.Now, _newSerialNo, SerialScope.Receiving);

                    _entities.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        private string GetNewReceivingId()
        {
            _newSerialNo = _serialManager.GetLastSerialNo(DateTime.Now, SerialScope.Receiving) + 1;

            DateTime currentDate = DateTime.Now.Date;

            string yearPart = currentDate.Year.ToString().Substring(2, 2);
            string monthPart = "00" + currentDate.Month;
            monthPart = monthPart.Substring(monthPart.Length - 2, 2);

            string dayPart = "00" + currentDate.Day;
            dayPart = dayPart.Substring(dayPart.Length - 2, 2);

            string datePart = yearPart + monthPart + dayPart;

            string serialNo = "0000" + _newSerialNo;

            string newId = datePart + ((int)SerialScope.Receiving) + serialNo.Substring(serialNo.Length - 4, 4);

            return newId;
        }
    }
}
