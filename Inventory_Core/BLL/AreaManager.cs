﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class AreaManager
    {
        private OffbeatInventoryEntities _entities;

        public AreaManager()
        {
            _entities = new OffbeatInventoryEntities();   
        }

        public List<Area> GetAreas(bool isActiveOnly)
        {
            var areas = new List<Area>();

            if (isActiveOnly)
            {
                areas = _entities.Areas.Where(i => i.IsActive == true).ToList();
            }
            else
            {
                areas = _entities.Areas.ToList();
            }

            return areas;
        }

        public Area GetArea(int areaCode)
        {
            var area = _entities.Areas.FirstOrDefault(a => a.AreaCode == areaCode);

            return area;
        }
    }
}
