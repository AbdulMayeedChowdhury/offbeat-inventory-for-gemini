﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Inventory_Core.BLL;
using Inventory_Core.DAL;
using System.Data.Objects.DataClasses;

namespace Inventory_GUI
{
    public partial class PurchaseOrderForm : BaseForm
    {
        private string _styleNo = string.Empty;

        private class OrderDetail
        {
            public string ItemCode { get; set; }
            public string ItemName { get; set; }
            public int SpecId { get; set; }
            public string SpecName { get; set; }
            public string Size { get; set; }
            public int MUnitId { get; set; }
            public string MUnitName { get; set; }
            public decimal Quantity { get; set; }
        }

        #region Constructor

        public PurchaseOrderForm()
        {
            InitializeComponent();
        }

        public PurchaseOrderForm(string styleNo) : this()
        {
            _styleNo = styleNo;
        }

        #endregion

        #region Event Handler

        private void PurchaseOrderForm_Load(object sender, EventArgs e)
        {
            ClearFields();

            if (_styleNo.Length > 0)
            {
                styleNoTextBox.Text = _styleNo;
                styleNoTextBox.Enabled = false;
                LoadStyleRelatedInformation(_styleNo);
            }
        }

        private void styleNoTextBox_Validating(object sender, CancelEventArgs e)
        {
            string styleNo = styleNoTextBox.Text.Trim();

            if (styleNo.Length == 0)
            {
                buyerLabel.Text = string.Empty;
                orderIdLabel.Text = string.Empty;

                orderDetailDataGridView.DataSource = null;
                poDataGridView.Rows.Clear();

                return;
            }

            LoadStyleRelatedInformation(styleNo);
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (styleNoTextBox.Text.Trim().Length == 0)
            {
                MessageBox.Show("Enter style number");
                styleNoTextBox.Focus();
                return;
            }

            if (poDataGridView.Rows.Count == 0)
            {
                MessageBox.Show("No data found to save.");
                styleNoTextBox.Focus();
                return;
            }

            var orderDetails = new EntityCollection<AccessoriesPurchaseOrderDetail>();

            decimal totalOrderQuantity = 0;

            for (int i = 0; i < poDataGridView.Rows.Count; i++)
            {
                var orderDetail = new AccessoriesPurchaseOrderDetail();

                orderDetail.AccessoriesId = poDataGridView.Rows[i].Cells["AccessoriesId"].Value.ToString();
                orderDetail.AttributeId = Convert.ToInt32(poDataGridView.Rows[i].Cells["SpecId"].Value);
                orderDetail.Size = Convert.ToString(poDataGridView.Rows[i].Cells["Size"].Value);
                orderDetail.MUnitId = Convert.ToInt32(poDataGridView.Rows[i].Cells["MeasurementId"].Value);

                if (Convert.ToString(poDataGridView.Rows[i].Cells["OrderQuantity"].Value) == string.Empty)
                {
                    orderDetail.Quantity = 0;
                }
                else
                {
                    orderDetail.Quantity = Convert.ToDecimal(poDataGridView.Rows[i].Cells["OrderQuantity"].Value);
                }

                totalOrderQuantity += orderDetail.Quantity;
                orderDetails.Add(orderDetail);
            }

            if (totalOrderQuantity == 0)
            {
                MessageBox.Show("No purchase order found to save.");
                return;
            }

            var newPurchaseOrder = new AccessoriesPurchaseOrder();

            newPurchaseOrder.PODate = orderDateTimePicker.Value;
            newPurchaseOrder.BuyerOrderId = orderIdLabel.Text.Trim();
            newPurchaseOrder.EntryUserId = LoginInformation.User.UserId;
            newPurchaseOrder.EntryDate = DateTime.Now;
            newPurchaseOrder.AccessoriesPurchaseOrderDetails = orderDetails;

            try
            {
                new PurchaseOrderManager().SavePurchaseOrder(newPurchaseOrder);

                poIdLabel.Text = newPurchaseOrder.POId;

                MessageBox.Show("New PO created successfully. PO Id: " + newPurchaseOrder.POId);

                ClearFields();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message);
                throw;
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            if (_styleNo.Length == 0)
            {
                ClearFields();
            }
        }

        #endregion

        #region Private Methods

        private void ClearFields()
        {
            styleNoTextBox.Text = string.Empty;
            buyerLabel.Text = string.Empty;
            orderIdLabel.Text = string.Empty;
            orderDateTimePicker.Value = DateTime.Now;

            orderDetailDataGridView.DataSource = null;
            
            poDataGridView.DataSource = null;
        }

        private void LoadStyleRelatedInformation(string styleNo)
        {
            BuyerOrder buyerOrder = new BuyerOrderManager().GetBuyerOrderByStyleNo(styleNo);

            if (buyerOrder == null)
            {
                buyerLabel.Text = string.Empty;
                orderIdLabel.Text = string.Empty;

                orderDetailDataGridView.DataSource = null;
                poDataGridView.Rows.Clear();

                return;
            }

            if (buyerOrder.IsClosed)
            {
                MessageBox.Show(@"This order is closed");
                buyerLabel.Text = string.Empty;
                orderIdLabel.Text = string.Empty;

                orderDetailDataGridView.DataSource = null;
                poDataGridView.Rows.Clear();

                return;
            }

            buyerLabel.Text = buyerOrder.Buyer.BuyerName;
            orderIdLabel.Text = buyerOrder.OrderId;

            var orderDetails = new List<OrderDetail>();

            foreach (var detail in buyerOrder.BuyerOrderDetails)
            {
                var orderDetail = new OrderDetail();

                orderDetail.ItemCode = detail.ItemCode;
                orderDetail.ItemName = detail.Item.ItemName;
                orderDetail.SpecId = detail.AttributeId;
                orderDetail.SpecName = detail.Attribute.AttributeName;
                orderDetail.Size = detail.Size;
                orderDetail.MUnitId = detail.MUnitId;
                orderDetail.MUnitName = detail.MeasurementUnit.MUnitName;
                orderDetail.Quantity = detail.Quantity;

                orderDetails.Add(orderDetail);
            }

            orderDetailDataGridView.AutoGenerateColumns = false;
            orderDetailDataGridView.DataSource = orderDetails;

            //foreach (var accessoryRequired in buyerOrder.BuyerOrderAccessoriesRequireds)
            //{
            //    poDataGridView.Rows.Add(accessoryRequired.AccessoriesId, accessoryRequired.Accessory.AccessoriesName,
            //                            accessoryRequired.AttributeId, accessoryRequired.Attribute.AttributeName,
            //                            accessoryRequired.MUnitId, accessoryRequired.MeasurementUnit.MUnitName,
            //                            accessoryRequired.Quantity);
            //}

            List<GetPurchaseOrderInformation_Result> poInformation = new PurchaseOrderManager().GetPurchasablesByStyleNo(styleNo);

            poDataGridView.AutoGenerateColumns = false;
            poDataGridView.DataSource = poInformation;
        }

        #endregion

    }
}
