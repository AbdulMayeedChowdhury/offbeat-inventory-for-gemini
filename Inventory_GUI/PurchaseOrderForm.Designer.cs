﻿namespace Inventory_GUI
{
    partial class PurchaseOrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.orderDetailDataGridView = new System.Windows.Forms.DataGridView();
            this.orderIdLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.styleNoTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buyerLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.poDataGridView = new System.Windows.Forms.DataGridView();
            this.poIdLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.orderDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.ItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemAttributeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemAttributeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemMeasurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccessoriesId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccessoriesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SpecId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SpecName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MeasurementId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Measurement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Required = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AlreadyOrdered = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailDataGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.poDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.orderDetailDataGridView);
            this.groupBox1.Controls.Add(this.orderIdLabel);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.styleNoTextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.buyerLabel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(849, 117);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Buyer Order Information";
            // 
            // orderDetailDataGridView
            // 
            this.orderDetailDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderDetailDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemCode,
            this.ItemName,
            this.ItemAttributeId,
            this.ItemAttributeName,
            this.ItemSize,
            this.ItemMeasurement,
            this.ItemQuantity});
            this.orderDetailDataGridView.Location = new System.Drawing.Point(336, 17);
            this.orderDetailDataGridView.Name = "orderDetailDataGridView";
            this.orderDetailDataGridView.Size = new System.Drawing.Size(510, 92);
            this.orderDetailDataGridView.TabIndex = 21;
            // 
            // orderIdLabel
            // 
            this.orderIdLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.orderIdLabel.Location = new System.Drawing.Point(64, 81);
            this.orderIdLabel.Name = "orderIdLabel";
            this.orderIdLabel.Size = new System.Drawing.Size(224, 21);
            this.orderIdLabel.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 81);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Order Id";
            // 
            // styleNoTextBox
            // 
            this.styleNoTextBox.Location = new System.Drawing.Point(64, 29);
            this.styleNoTextBox.Name = "styleNoTextBox";
            this.styleNoTextBox.Size = new System.Drawing.Size(100, 20);
            this.styleNoTextBox.TabIndex = 17;
            this.styleNoTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.styleNoTextBox_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Style No.";
            // 
            // buyerLabel
            // 
            this.buyerLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.buyerLabel.Location = new System.Drawing.Point(64, 55);
            this.buyerLabel.Name = "buyerLabel";
            this.buyerLabel.Size = new System.Drawing.Size(266, 21);
            this.buyerLabel.TabIndex = 15;
            this.buyerLabel.Text = "Buyer";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Buyer";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.poDataGridView);
            this.groupBox2.Location = new System.Drawing.Point(13, 168);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(943, 347);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "PO Information";
            // 
            // poDataGridView
            // 
            this.poDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.poDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AccessoriesId,
            this.AccessoriesName,
            this.SpecId,
            this.SpecName,
            this.Size,
            this.MeasurementId,
            this.Measurement,
            this.Required,
            this.AlreadyOrdered,
            this.OrderQuantity});
            this.poDataGridView.Location = new System.Drawing.Point(21, 30);
            this.poDataGridView.Name = "poDataGridView";
            this.poDataGridView.Size = new System.Drawing.Size(904, 304);
            this.poDataGridView.TabIndex = 29;
            // 
            // poIdLabel
            // 
            this.poIdLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.poIdLabel.Location = new System.Drawing.Point(99, 19);
            this.poIdLabel.Name = "poIdLabel";
            this.poIdLabel.Size = new System.Drawing.Size(151, 21);
            this.poIdLabel.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "PO Id";
            // 
            // orderDateTimePicker
            // 
            this.orderDateTimePicker.Location = new System.Drawing.Point(536, 19);
            this.orderDateTimePicker.Name = "orderDateTimePicker";
            this.orderDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.orderDateTimePicker.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(432, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Order Date";
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(868, 69);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(91, 33);
            this.clearButton.TabIndex = 22;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(868, 109);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(91, 33);
            this.exitButton.TabIndex = 21;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(868, 30);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(91, 33);
            this.saveButton.TabIndex = 20;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // ItemCode
            // 
            this.ItemCode.DataPropertyName = "ItemCode";
            this.ItemCode.HeaderText = "Code";
            this.ItemCode.Name = "ItemCode";
            this.ItemCode.ReadOnly = true;
            this.ItemCode.Visible = false;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.HeaderText = "Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // ItemAttributeId
            // 
            this.ItemAttributeId.DataPropertyName = "SpecId";
            this.ItemAttributeId.HeaderText = "Spec. Id";
            this.ItemAttributeId.Name = "ItemAttributeId";
            this.ItemAttributeId.ReadOnly = true;
            this.ItemAttributeId.Visible = false;
            // 
            // ItemAttributeName
            // 
            this.ItemAttributeName.DataPropertyName = "SpecName";
            this.ItemAttributeName.HeaderText = "Spec/Color";
            this.ItemAttributeName.Name = "ItemAttributeName";
            this.ItemAttributeName.ReadOnly = true;
            // 
            // ItemSize
            // 
            this.ItemSize.DataPropertyName = "Size";
            this.ItemSize.HeaderText = "Size";
            this.ItemSize.Name = "ItemSize";
            // 
            // ItemMeasurement
            // 
            this.ItemMeasurement.DataPropertyName = "MUnitName";
            this.ItemMeasurement.HeaderText = "Unit";
            this.ItemMeasurement.Name = "ItemMeasurement";
            this.ItemMeasurement.ReadOnly = true;
            // 
            // ItemQuantity
            // 
            this.ItemQuantity.DataPropertyName = "Quantity";
            this.ItemQuantity.HeaderText = "Quantity";
            this.ItemQuantity.Name = "ItemQuantity";
            this.ItemQuantity.ReadOnly = true;
            // 
            // AccessoriesId
            // 
            this.AccessoriesId.DataPropertyName = "AccessoriesId";
            this.AccessoriesId.HeaderText = "Accessories Id";
            this.AccessoriesId.Name = "AccessoriesId";
            this.AccessoriesId.ReadOnly = true;
            this.AccessoriesId.Visible = false;
            // 
            // AccessoriesName
            // 
            this.AccessoriesName.DataPropertyName = "AccessoriesName";
            this.AccessoriesName.HeaderText = "Accessories Name";
            this.AccessoriesName.Name = "AccessoriesName";
            this.AccessoriesName.ReadOnly = true;
            // 
            // SpecId
            // 
            this.SpecId.DataPropertyName = "AttributeId";
            this.SpecId.HeaderText = "Spec Id";
            this.SpecId.Name = "SpecId";
            this.SpecId.ReadOnly = true;
            this.SpecId.Visible = false;
            // 
            // SpecName
            // 
            this.SpecName.DataPropertyName = "AttributeName";
            this.SpecName.HeaderText = "Spec/Color";
            this.SpecName.Name = "SpecName";
            this.SpecName.ReadOnly = true;
            // 
            // Size
            // 
            this.Size.DataPropertyName = "Size";
            this.Size.HeaderText = "Size";
            this.Size.Name = "Size";
            // 
            // MeasurementId
            // 
            this.MeasurementId.DataPropertyName = "MUnit";
            this.MeasurementId.HeaderText = "Measurement Id";
            this.MeasurementId.Name = "MeasurementId";
            this.MeasurementId.ReadOnly = true;
            this.MeasurementId.Visible = false;
            // 
            // Measurement
            // 
            this.Measurement.DataPropertyName = "MUnitName";
            this.Measurement.HeaderText = "Unit";
            this.Measurement.Name = "Measurement";
            this.Measurement.ReadOnly = true;
            // 
            // Required
            // 
            this.Required.DataPropertyName = "RequiredQuantity";
            this.Required.HeaderText = "Required";
            this.Required.Name = "Required";
            this.Required.ReadOnly = true;
            // 
            // AlreadyOrdered
            // 
            this.AlreadyOrdered.DataPropertyName = "POQuantity";
            this.AlreadyOrdered.HeaderText = "Already Ordered";
            this.AlreadyOrdered.Name = "AlreadyOrdered";
            this.AlreadyOrdered.ReadOnly = true;
            // 
            // OrderQuantity
            // 
            this.OrderQuantity.HeaderText = "Order Quantity";
            this.OrderQuantity.Name = "OrderQuantity";
            // 
            // PurchaseOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 514);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.orderDateTimePicker);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.poIdLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "PurchaseOrderForm";
            this.Text = "PurchaseOrderForm";
            this.Load += new System.EventHandler(this.PurchaseOrderForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailDataGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.poDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox styleNoTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label buyerLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label poIdLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker orderDateTimePicker;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.DataGridView poDataGridView;
        private System.Windows.Forms.Label orderIdLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView orderDetailDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemAttributeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemAttributeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemMeasurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccessoriesId;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccessoriesName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SpecId;
        private System.Windows.Forms.DataGridViewTextBoxColumn SpecName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn MeasurementId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Measurement;
        private System.Windows.Forms.DataGridViewTextBoxColumn Required;
        private System.Windows.Forms.DataGridViewTextBoxColumn AlreadyOrdered;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderQuantity;
    }
}