﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventory_Core.DAL;

namespace Inventory_Core.BLL
{
    public class DeliveryManager
    {
        private OffbeatInventoryEntities _entities;
        private int newSerialNo;
        private SerialManager _serialManager;

        public DeliveryManager()
        {
            _entities = new OffbeatInventoryEntities();
        }

        public AccessoriesDeliveredToProduction GetDelivery(string deliveryId)
        {
            var delivery = _entities.AccessoriesDeliveredToProductions.FirstOrDefault(d => d.DeliveryId == deliveryId);

            return delivery;
        }

        public List<AccessoriesDeliveredToProduction> GetDeliveriesByStyleNo(string styleNo)
        {
            var buyerOrder = _entities.BuyerOrders.FirstOrDefault(o => o.StyleNo == styleNo);

            if (buyerOrder == null)
            {
                return null;
            }

            var deliveries = _entities.AccessoriesDeliveredToProductions.Where(d => d.BuyerOrderId == buyerOrder.OrderId).ToList();

            return deliveries;
        }

        public void SaveDelivery(AccessoriesDeliveredToProduction newDelivery)
        {
            try
            {
                using (_entities)
                {
                    _serialManager = new SerialManager(_entities);
                    var stockManager = new AccessoriesStockManager(_entities);

                    newDelivery.DeliveryId = GetNewDeliveryId();

                    newDelivery.FromArea = 1;
                    newDelivery.ToArea = 2;
                    newDelivery.IsActive = true;

                    _entities.AddToAccessoriesDeliveredToProductions(newDelivery);

                    foreach (var detail in newDelivery.AccessoriesDeliveredToProductionDetails)
                    {
                        stockManager.DecreaseStock(detail.AccessoriesId, detail.AttributeId, detail.Size, detail.MUnitId, newDelivery.FromArea, detail.Quantity);
                        stockManager.IncreaseStock(detail.AccessoriesId, detail.AttributeId, detail.Size, detail.MUnitId, newDelivery.ToArea, detail.Quantity);
                    }

                    _serialManager.UpdateSerialNo(DateTime.Now, newSerialNo, SerialScope.Delivery);

                    _entities.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        private string GetNewDeliveryId()
        {
            newSerialNo = _serialManager.GetLastSerialNo(DateTime.Now, SerialScope.Delivery) + 1;

            DateTime currentDate = DateTime.Now.Date;

            string yearPart = currentDate.Year.ToString().Substring(2, 2);
            string monthPart = "00" + currentDate.Month.ToString();
            monthPart = monthPart.Substring(monthPart.Length - 2, 2);

            string dayPart = "00" + currentDate.Day.ToString();
            dayPart = dayPart.Substring(dayPart.Length - 2, 2);

            string datePart = yearPart + monthPart + dayPart;

            string serialNo = "0000" + newSerialNo.ToString();

            string newId = datePart + ((int)SerialScope.Delivery).ToString() + serialNo.Substring(serialNo.Length - 4, 4);

            return newId;
        }

        public List<GetDeliveryInformation_Result> GetDeliverablesByStyleNo(string styleNo)
        {
            List<GetDeliveryInformation_Result> result;

            using (_entities)
            {
                result = _entities.GetDeliveryInformation(styleNo).ToList();
            }

            return result;
        }
    }
}
