﻿namespace Inventory_GUI
{
    partial class AccessoriesStockReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.showButton = new System.Windows.Forms.Button();
            this.areaComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.OffbeatInventoryDataSet = new Inventory_GUI.OffbeatInventoryDataSet();
            this.AreaWiseAccessoriesStockBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.AreaWiseAccessoriesStockTableAdapter = new Inventory_GUI.OffbeatInventoryDataSetTableAdapters.AreaWiseAccessoriesStockTableAdapter();
            this.AreaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            //this.AreaTableAdapter = new Inventory_GUI.OffbeatInventoryDataSetTableAdapters.AreaTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.OffbeatInventoryDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AreaWiseAccessoriesStockBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AreaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            reportDataSource3.Name = "AccessoriesStockDataSet";
            reportDataSource3.Value = this.AreaWiseAccessoriesStockBindingSource;
            reportDataSource4.Name = "AreaDataSet";
            reportDataSource4.Value = this.AreaBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource3);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource4);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Inventory_GUI.Report.AccessoriesStockReport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(12, 43);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(984, 456);
            this.reportViewer1.TabIndex = 0;
            // 
            // showButton
            // 
            this.showButton.Location = new System.Drawing.Point(221, 14);
            this.showButton.Name = "showButton";
            this.showButton.Size = new System.Drawing.Size(75, 23);
            this.showButton.TabIndex = 1;
            this.showButton.Text = "Show";
            this.showButton.UseVisualStyleBackColor = true;
            this.showButton.Click += new System.EventHandler(this.showButton_Click);
            // 
            // areaComboBox
            // 
            this.areaComboBox.FormattingEnabled = true;
            this.areaComboBox.Location = new System.Drawing.Point(84, 14);
            this.areaComboBox.Name = "areaComboBox";
            this.areaComboBox.Size = new System.Drawing.Size(121, 21);
            this.areaComboBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Area";
            // 
            // OffbeatInventoryDataSet
            // 
            this.OffbeatInventoryDataSet.DataSetName = "OffbeatInventoryDataSet";
            this.OffbeatInventoryDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // AreaWiseAccessoriesStockBindingSource
            // 
            this.AreaWiseAccessoriesStockBindingSource.DataMember = "AreaWiseAccessoriesStock";
            this.AreaWiseAccessoriesStockBindingSource.DataSource = this.OffbeatInventoryDataSet;
            // 
            // AreaWiseAccessoriesStockTableAdapter
            // 
            this.AreaWiseAccessoriesStockTableAdapter.ClearBeforeFill = true;
            // 
            // AreaBindingSource
            // 
            this.AreaBindingSource.DataMember = "Area";
            this.AreaBindingSource.DataSource = this.OffbeatInventoryDataSet;
            // 
            // AreaTableAdapter
            // 
            //this.AreaTableAdapter.ClearBeforeFill = true;
            // 
            // AccessoriesStockReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 504);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.areaComboBox);
            this.Controls.Add(this.showButton);
            this.Controls.Add(this.reportViewer1);
            this.Name = "AccessoriesStockReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AccessoriesStockReportForm";
            this.Load += new System.EventHandler(this.AccessoriesStockReportForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.OffbeatInventoryDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AreaWiseAccessoriesStockBindingSource)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.AreaBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.Button showButton;
        private System.Windows.Forms.ComboBox areaComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource AreaWiseAccessoriesStockBindingSource;
        private OffbeatInventoryDataSet OffbeatInventoryDataSet;
        private System.Windows.Forms.BindingSource AreaBindingSource;
        private OffbeatInventoryDataSetTableAdapters.AreaWiseAccessoriesStockTableAdapter AreaWiseAccessoriesStockTableAdapter;
        //private OffbeatInventoryDataSetTableAdapters.AreaTableAdapter AreaTableAdapter;
    }
}