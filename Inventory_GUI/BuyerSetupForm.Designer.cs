﻿namespace Inventory_GUI
{
    partial class BuyerSetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buyerDataGridView = new System.Windows.Forms.DataGridView();
            this.BuyerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.buyerNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.buyerDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // buyerDataGridView
            // 
            this.buyerDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.buyerDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BuyerId,
            this.BuyerName});
            this.buyerDataGridView.Location = new System.Drawing.Point(21, 90);
            this.buyerDataGridView.Name = "buyerDataGridView";
            this.buyerDataGridView.Size = new System.Drawing.Size(289, 244);
            this.buyerDataGridView.TabIndex = 17;
            // 
            // BuyerId
            // 
            this.BuyerId.DataPropertyName = "BuyerId";
            this.BuyerId.HeaderText = "Id";
            this.BuyerId.Name = "BuyerId";
            this.BuyerId.ReadOnly = true;
            // 
            // BuyerName
            // 
            this.BuyerName.DataPropertyName = "BuyerName";
            this.BuyerName.HeaderText = "Name";
            this.BuyerName.Name = "BuyerName";
            this.BuyerName.ReadOnly = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(235, 47);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 16;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(156, 47);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 15;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // buyerNameTextBox
            // 
            this.buyerNameTextBox.Location = new System.Drawing.Point(82, 21);
            this.buyerNameTextBox.Name = "buyerNameTextBox";
            this.buyerNameTextBox.Size = new System.Drawing.Size(228, 20);
            this.buyerNameTextBox.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Buyer Name";
            // 
            // BuyerSetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 346);
            this.Controls.Add(this.buyerDataGridView);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.buyerNameTextBox);
            this.Controls.Add(this.label1);
            this.Name = "BuyerSetupForm";
            this.Text = "Buyer Setup";
            this.Load += new System.EventHandler(this.BuyerSetupForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.buyerDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.TextBox buyerNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView buyerDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuyerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuyerName;
    }
}